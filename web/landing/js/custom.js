/*global $,window,WOW,setTimeout*/

$(function () {

    "use strict";

    // Variables

    var loading = $(".loading"),
        scroll = $(".scroll"),
        htm_bod = $("html, body"),
        switcher = $(".switcher"),
        gradientsli = $(".switcher .gradients span"),
        cog = $(".switcher .cog"),
        body = $("body"),
        dw_section = $("#contact"),
        Counter = $(".counter"),
        download = $(".js-join"),
        screenshots = $(".screenshots-carousel"),
        testimonials = $(".testimonials-carousel"),
        blog = $(".blog-carousel"),
        team = $(".team-carousel"),
        send = $(".send"),
        output = $(".output"),
        form = $("#mach-form"),
        messagecontainer = $("#message-newsletter"),
        navbar = $(".navbar-default"),
        nva_li = $(".navbar-default .navbar-nav > li > a, .dropdown-menu li a"),
        video = $(".video");

    // Loading

    $(window).on("load", function () {

        loading.fadeOut();

    });

    // Show Scroll

    $(window).on("scroll", function () {

        if ($(this).scrollTop() > 600) {

            scroll.fadeIn();

        } else {

            scroll.fadeOut();

        }

    });

    // Scroll To Top

    scroll.on("click", function () {

        htm_bod.animate({

            scrollTop: 0

        }, 1500, 'easeInOutExpo');

    });

    // Colors Switcher

    cog.on("click", function () {

        switcher.toggleClass('active');

    });

    gradientsli.click(function () {

        body.attr("data-default-theme", $(this).attr("data-theme"));

        $(this).addClass("active").siblings().removeClass("active");

    });

    // Download

    download.on("click", function (e) {

        htm_bod.animate({

            scrollTop: dw_section.offset().top

        }, 1500, 'easeInOutExpo');

        e.preventDefault();

    });

    // CounterUp

    Counter.counterUp({
        delay: 10,
        time: 3000
    });

    // Screenshots

    screenshots.owlCarousel({
        items: 4,
        margin: 10,
        dots: true,
        autoplay: true,
        loop: true,
        responsiveClass: true,
        responsive: {0: {items: 1}, 500: {items: 2}, 1000: {items: 3}, 1200: {items: 4}}
    });

    // Testimonials

    testimonials.owlCarousel({
        items: 1,
        dots: false,
        loop: true,
        nav: true,
        dots: true,
        autoplay: true,
        navText: ["<i class='fa fa-angle-left'>", "<i class='fa fa-angle-right'>"]
    });

    // Team

    team.owlCarousel({
        items: 4,
        margin: 10,
        dots: true,
        autoplay: true,
        loop: true,
        responsiveClass: true,
        responsive: {0: {items: 1}, 500: {items: 2}, 1000: {items: 3}, 1200: {items: 4}}
    });

    // Blog

    blog.owlCarousel({
        items: 3,
        margin: 10,
        dots: true,
        autoplay: true,
        loop: true,
        responsiveClass: true,
        responsive: {0: {items: 1}, 500: {items: 1}, 1000: {items: 2}, 1200: {items: 3}}
    });

    // Contact Form

    send.on("click", function () {

        var name = $(".name").val(),
            email = $(".email").val(),
            subject = $(".subject").val(),
            message = $(".message").val(),
            dataString = 'name=' + name + '&email=' + email + '&subject=' + subject + '&message=' + message;

        function validateEmail(emailadd) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(emailadd);
        }

        if (name === '' || email === '' || subject === '' || message === '') {

            output.html("<div class='alert alert-warning'>Пожалуйста, заполните все обязательные поля.</div>").fadeIn().delay(3000).fadeOut();

            // } else if (!validateEmail(email)) {
            //     output.html("<div class='alert alert-warning'>write a valid email.</div>").fadeIn().delay(3000).fadeOut();

        } else {

            $.ajax({
                url: "/site/contact",
                type: "POST",
                data: dataString,
                cache: false,
                success: function (result) {
                    console.log(result);
                    if (result == 1) {
                        output.html("<div class='alert alert-success'>Сообщение успешно отправлено.</div>").fadeIn().delay(3000).fadeOut();
                        var name = $(".name").val(''),
                            email = $(".email").val(''),
                            subject = $(".subject").val(''),
                            message = $(".message").val('');
                    } else {
                        output.html("<div class='alert alert-danger'>Сообщение не отправлено. Пожалуйста, заполните все обязательные поля и докажите, что Вы не робот.</div>").fadeIn().delay(3000).fadeOut();
                    }
                }
            });
        }
        return false;
    });

    // AjaxChimp

    /*if (form.length) {
        form.ajaxChimp({
            callback: mailchimpCallback,
            url: "https://hopelabs.us17.list-manage.com/subscribe/post?u=d95e5b073da969524c5fa4423&amp;id=3480e9242e"
        });
    }

    function mailchimpCallback(resp) {
        messagecontainer.removeClass('error');

        form.find(".form-group").removeClass("error");
        if (resp.result === "error") {
            form.find(".input-group").addClass("error");
            messagecontainer.addClass("error");
        } else {
            form.find(".form-control").fadeIn().val("");
        }

        messagecontainer.slideDown("slow", "swing");

        setTimeout(function () {
            messagecontainer.slideUp("slow", "swing");
        }, 10000);
    }*/

    // Add Class To Navbar

    $(window).on("scroll", function () {

        if ($(this).scrollTop() > navbar.height()) {

            navbar.addClass("sticky-nav gradient-opacity");

        } else {

            navbar.removeClass("sticky-nav gradient-opacity");

        }

    });

    // Smooth Scroll

    nva_li.on("click", function (e) {

        htm_bod.animate({

            scrollTop: $($(this).attr("href")).offset().top

        }, 1500, 'easeInOutExpo');

        e.preventDefault();

    });

    // Popup Video

    video.magnificPopup({
        type: 'iframe'
    });

});