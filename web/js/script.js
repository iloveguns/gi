$(document).ready(function () {
    $('.popup-gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        mainClass: 'mfp-img-mobile',
        tLoading: 'Загрузка изображения #%curr%...',
        tClose: 'Закрыть',
        gallery: {
            enabled: true,
            navigateByImgClick: true,
            tPrev: 'Предыдущая',
            tNext: 'Следующая',
            tCounter: '<span class="mfp-counter">%curr% из %total%</span>'
        },
        image: {
            tError: '<a href="%url%">Изображение #%curr%</a> невозможно открыть.',
            titleSrc: function (item) {
                return item.el.attr('title');
            }
        }
    });
});