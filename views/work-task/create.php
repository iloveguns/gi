<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\WorkTask */

$this->title = 'Create Work Task';
$this->params['breadcrumbs'][] = ['label' => 'Work Tasks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-task-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
