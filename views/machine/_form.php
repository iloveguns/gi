<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;
use app\models\data\Machine;
use app\models\data\User;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use zxbodya\yii2\galleryManager\GalleryManager;

/* @var $this yii\web\View */
/* @var $model app\models\data\Machine */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-default">
    <div class="box-body">
        <div class="machine-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'type')->dropDownList(Machine::getTypeList()) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'work_type')->dropDownList(Machine::getWorkTypeList()) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-8">
                    <?= $form->field($model, 'placement')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'inventory_number')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'status')->dropDownList(Machine::getStatusList()) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'startDateStr')->widget(DatePicker::class, [
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy'
                        ]
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model,
                        'responsible_user_id')
                        ->widget(Select2::class, [
                            'data' => ArrayHelper::map(User::getUserListByRole(User::ROLE_ENGINEER), 'id',
                                'profile.nameWithPosition'),
                            'options' => ['placeholder' => 'Выберите ответственного за осмотры'],
                        ]) ?>
                </div>
            </div>
            <div class="row">
                <?php if ($model->work_type == Machine::WORK_TYPE_DETAILS_COUNT): ?>
                    <div class="col-md-4">
                        <?= $form->field($model, 'produced_count')->textInput() ?>
                    </div>
                <?php endif; ?>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?php if ($model->isNewRecord): ?>
                            <p>Вы сможете прикрепить фотографии после сохранения.</p>
                        <?php else: ?>
                            <label>Фотографии</label>
                            <?= GalleryManager::widget([
                                'model' => $model,
                                'behaviorName' => 'galleryBehavior',
                                'apiRoute' => 'work/gallery-api'
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
