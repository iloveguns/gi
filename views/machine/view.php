<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\data\RepairTask;
use app\models\data\PlannedWork;
use app\models\data\Machine;
use app\models\data\Work;
use app\models\data\User;
use app\models\data\Inspection;
use app\models\data\Examination;
use app\models\data\Task;

/* @var $this yii\web\View */
/* @var $model app\models\data\Machine */

/* @var $plannedWorkSearchModel \app\models\search\PlannedWorkSearch */
/* @var $workSearchModel \app\models\search\WorkSearch */
/* @var $examinationSearchModel \app\models\search\ExaminationSearch */
/* @var $inspectionsSearchModel app\models\search\InspectionSearch */
/* @var $repairTasksSearchModel app\models\search\RepairTaskSearch */

/* @var $plannedWorkDataProvider yii\data\ActiveDataProvider */
/* @var $workDataProvider yii\data\ActiveDataProvider */
/* @var $examinationDataProvider yii\data\ActiveDataProvider */
/* @var $inspectionsDataProvider yii\data\ActiveDataProvider */
/* @var $repairTasksDataProvider yii\data\ActiveDataProvider */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$formatter = Yii::$app->formatter;
$images = $model->getBehavior('galleryBehavior')->getImages();
?>
<div class="machine-view">

    <p>
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены?',
                    'method' => 'post',
                ],
            ]) ?>
            <?= Html::a('Получить QR-код', ['qr-code', 'id' => $model->id], ['class' => 'btn btn-default']) ?>
        <?php endif; ?>
        <?php if (Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
            <?= Html::a('Текущие задачи ГИ',
                [
                    '/task/index',
                    'TaskSearch' => [
                        'user_id' => Yii::$app->user->id,
                        'machine_id' => $model->id,
                        'status' => [Task::STATUS_NOT_COMPLETED, Task::STATUS_PROCESS, Task::STATUS_TEST]
                    ]
                ],
                ['class' => 'btn btn-info']) ?>
        <?php endif; ?>
        <?php if (Yii::$app->user->can(User::ROLE_ENGINEER)): ?>
            <?= Html::a('Текущие задачи',
                [
                    '/task/index',
                    'TaskSearch' => [
                        'user_id' => Yii::$app->user->id,
                        'machine_id' => $model->id,
                        'status' => [Task::STATUS_NOT_COMPLETED, Task::STATUS_PROCESS]
                    ]
                ],
                ['class' => 'btn btn-info']) ?>
        <?php endif; ?>
    </p>

    <?php $columns = [
        'typeName',
        'workTypeName',
        'name',
        'placement',
        'inventory_number',
        'start_date:date',
        [
            'attribute' => 'responsible_user_id',
            'value' => function (Machine $model) {
                return $model->responsibleUser->profile->name;
            }
        ],
        [
            'attribute' => 'status',
            'format' => 'raw',
            'value' => function (Machine $model) {
                $labelClass = 'success';
                switch ($model->status) {
                    case Machine::STATUS_NOT_ACTIVE:
                        $labelClass = 'danger';
                        break;
                    case Machine::STATUS_ACTIVE:
                        $labelClass = 'success';
                        break;
                }
                return Html::tag('span', $model->getStatusName(), ['class' => 'label label-' . $labelClass]);
            }
        ]
    ]; ?>

    <?php if ($model->work_type == Machine::WORK_TYPE_DETAILS_COUNT): ?>
        <?php $columns[] = [
            'attribute' => 'produced_count',
            'value' => function (Machine $model) {
                return $model->produced_count . ' шт.';
            }
        ]; ?>

    <?php endif; ?>

    <div class="box box-default">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => $columns,
            ]) ?>
        </div>
    </div>

    <?php if (!empty($images)): ?>
        <h3>Фотографии оборудования</h3>
        <div class="box box-default">
            <div class="box-body">
                <div class="popup-gallery">
                    <?php foreach ($images as $image): ?>
                        <?= Html::a(Html::img($image->getUrl('preview'),
                            ['width' => 100, 'height' => 100, 'style' => 'object-fit: cover;']),
                            $image->getUrl('medium'), ['title' => $image->description]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <h3>Освидетельствования</h3>
    <p>
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN) || Yii::$app->user->id == $model->responsible_user_id): ?>
            <?= Html::a('Провести освидетельствование', ['/examination/create', 'machine_id' => $model->id],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <div class="box box-default">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $examinationDataProvider,
//            'filterModel' => $examinationSearchModel,
                'options' => [
                    'class' => 'table-responsive',
                ],
                'columns' => [
                    'date:date',
                    'document_number',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function (Examination $model) {
                            $labelClass = $model->status ? 'success' : 'warning';
                            return Html::tag('span', $model->getStatusName(),
                                ['class' => 'label label-' . $labelClass]);
                        },
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, Examination $model) {
                                return Html::a('Подробнее',
                                    ['/examination/view', 'id' => $model->id], ['class' => 'btn btn-primary']);
                            },
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <?php if ($model->work_type == Machine::WORK_TYPE_DETAILS_COUNT || $model->work_type == Machine::WORK_TYPE_PERIODS): ?>
        <hr>
        <h3>Регламент плановых ТО</h3>
        <p>
            <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
                <?= Html::a('Добавить плановое ТО', ['/planned-work/create', 'machine_id' => $model->id],
                    ['class' => 'btn btn-success']) ?>
            <?php endif; ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $plannedWorkDataProvider,
                    'options' => [
                        'class' => 'table-responsive',
                    ],
//            'filterModel' => $plannedWorkSearchModel,
                    'columns' => [
                        'id',
                        'frequencyStr',
                        [
                            'attribute' => 'tasksStr',
                            'format' => 'raw',
                            'value' => function (PlannedWork $model) {
                                return $model->getTasksStr('<br>');
                            }
                        ],
                        'notes',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, PlannedWork $model) {
                                    return Html::a('Подробнее',
                                        ['/planned-work/view', 'id' => $model->id], ['class' => 'btn btn-primary']);
                                },
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>

        <h3>Проведение ТО</h3>
        <p>
            <?php if (Yii::$app->user->can(User::ROLE_ENGINEER)): ?>
                <?= Html::a('Провести ТО', ['/work/create', 'machine_id' => $model->id],
                    ['class' => 'btn btn-success']) ?>
            <?php endif; ?>
        </p>
        <div class="box box-default">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $workDataProvider,
                    'options' => [
                        'class' => 'table-responsive',
                    ],
//            'filterModel' => $workSearchModel,
                    'columns' => [
                        [
                            'filter' => false,
                            'format' => 'raw',
                            'label' => 'Запланировано к выполнению (дата/изготовлено шт)',
                            'value' => function (Work $model) {
                                return $model->getPlannedAtStr();
                            }
                        ],
                        'started_at:datetime',
                        [
                            'attribute' => 'work_user_id',
                            'label' => 'Исполнитель',
                            'value' => 'workUser.profile.name',
                        ],
                        [
                            'attribute' => 'type',
                            'value' => 'typeName',
                        ],
                        [
                            'attribute' => 'status',
                            'format' => 'raw',
                            'filter' => Work::getStatusList(),
                            'value' => function (Work $model) {
                                $labelClass = 'success';
                                switch ($model->status) {
                                    case Work::STATUS_CREATED:
                                        $labelClass = 'danger';
                                        break;
                                    case Work::STATUS_IN_PROCESS:
                                        $labelClass = 'primary';
                                        break;
                                    case Work::STATUS_TEST:
                                        $labelClass = 'warning';
                                        break;
                                    case Work::STATUS_COMPLETED:
                                        $labelClass = 'success';
                                        break;
                                    case Work::STATUS_REJECTED:
                                        $labelClass = 'danger';
                                        break;
                                }
                                return Html::tag('span', $model->getStatusName(),
                                    ['class' => 'label label-' . $labelClass]);
                            }
                        ],
                        [
                            'attribute' => 'progress',
                            'format' => 'raw',
                            'value' => function (Work $model) {
                                return $model->type == Work::TYPE_PLANNED
                                    ? Html::tag('span', $model->getProgress() . '%', ['class' => 'badge bg-light-blue'])
                                    : null;
                            }
                        ],
                        'ended_at:datetime',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view}',
                            'buttons' => [
                                'view' => function ($url, Work $model) {
                                    return Html::a('Подробнее',
                                        ['/work/view', 'id' => $model->id], ['class' => 'btn btn-primary']);
                                },
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    <?php endif; ?>

    <h3>Осмотры</h3>
    <p>
        <?php if (Yii::$app->user->can(User::ROLE_ENGINEER)): ?>
            <?= Html::a('Провести осмотр', ['/inspection/create', 'machine_id' => $model->id],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>
    <div class="box box-default">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $inspectionsDataProvider,
//            'filterModel' => $inspectionsSearchModel,
                'options' => [
                    'class' => 'table-responsive',
                ],
                'columns' => [
                    'created_at:date',
                    [
                        'attribute' => 'user_id',
                        'label' => 'Исполнитель',
                        'value' => 'user.profile.name',
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function (Inspection $model) use ($formatter) {
                            $labelClass = $model->status ? 'primary' : 'warning';
                            return Html::tag('span', $model->getStatusName(),
                                ['class' => 'label label-' . $labelClass]);
                        }
                    ],
                    [
                        'attribute' => 'is_problem_found',
                        'format' => 'raw',
                        'value' => function (Inspection $model) use ($formatter) {
                            $labelClass = $model->is_problem_found ? 'danger' : 'success';
                            return Html::tag('span', $formatter->asBoolean($model->is_problem_found),
                                ['class' => 'label label-' . $labelClass]);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, Inspection $model) {
                                return Html::a('Подробнее',
                                    ['/inspection/view', 'id' => $model->id], ['class' => 'btn btn-primary']);
                            },
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>

    <h3>Задания</h3>
    <p>
        <?php if (Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
            <?= Html::a('Добавить задание', ['/repair-task/create', 'machine_id' => $model->id],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>
    <div class="box box-default">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $repairTasksDataProvider,
//            'filterModel' => $repairTasksSearchModel,
                'options' => [
                    'class' => 'table-responsive',
                ],
                'columns' => [
                    'id',
                    'created_at:datetime',
                    [
                        'attribute' => 'work_user_id',
                        'label' => 'Исполнитель',
                        'value' => 'workUser.profile.name',
                    ],
                    'planned_ended_at:datetime',
                    'started_at:datetime',
                    'ended_at:datetime',
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'filter' => RepairTask::getStatusList(),
                        'value' => function (RepairTask $model) {
                            $labelClass = 'success';
                            switch ($model->status) {
                                case RepairTask::STATUS_CREATED:
                                    $labelClass = 'danger';
                                    break;
                                case RepairTask::STATUS_IN_PROCESS:
                                    $labelClass = 'primary';
                                    break;
                                case RepairTask::STATUS_TEST:
                                    $labelClass = 'warning';
                                    break;
                                case RepairTask::STATUS_COMPLETED:
                                    $labelClass = 'success';
                                    break;
                                case RepairTask::STATUS_REJECTED:
                                    $labelClass = 'danger';
                                    break;
                            }
                            return Html::tag('span', $model->getStatusName(),
                                ['class' => 'label label-' . $labelClass]);
                        }
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, RepairTask $model) {
                                return Html::a('Подробнее',
                                    ['/repair-task/view', 'id' => $model->id], ['class' => 'btn btn-primary']);
                            },
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
