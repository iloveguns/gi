<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\Machine */

$this->title = 'Добавить оборудование';
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="machine-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
