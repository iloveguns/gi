<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\Machine */

$this->title = 'Получить QR-код';
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getName(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('js/qrcode.min.js');
?>

<div id="qrcode"></div>

<?php
// Генерация QR-кода
$this->registerJs('
    var qrcode = new QRCode(document.getElementById("qrcode"), {
        width: 150,
        height: 150
    });
    qrcode.makeCode("' . $model->getUrl() . '");
    
    var $qrcode = $("#qrcode");
    var $qrcodeImg = $qrcode.find("img");
    
    ', \yii\web\View::POS_READY);
?>
