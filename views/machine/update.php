<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\Machine */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->getName(), 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="machine-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
