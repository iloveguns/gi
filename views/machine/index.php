<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\data\Machine;
use kartik\date\DatePicker;
use app\models\data\User;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\MachineSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Оборудование';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="machine-index">

    <p>
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?= Html::a('Добавить оборудование', ['create'], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <div class="box box-default">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'options' => [
                    'class' => 'table-responsive',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'type',
                        'filter' => Machine::getTypeList(),
                        'value' => 'typeName'
                    ],
                    [
                        'attribute' => 'work_type',
                        'filter' => Machine::getWorkTypeList(),
                        'value' => 'WorkTypeName',
                    ],
                    [
                        'attribute' => 'name',
                        'format' => 'raw',
                        'value' => function (Machine $model) {
                            return Html::a($model->getName(),
                                ['view', 'id' => $model->id]);
                        },
                    ],
                    [
                        'attribute' => 'start_date',
                        'format' => 'date',
                        'filter' => DatePicker::widget([
                            'model' => $searchModel,
                            'attribute' => 'start_date',
                            'pluginOptions' => [
                                'autoclose' => true,
                                'format' => 'dd.mm.yyyy'
                            ]
                        ])
                    ],
                    [
                        'attribute' => 'status',
                        'filter' => Machine::getStatusList(),
                        'format' => 'raw',
                        'value' => function (Machine $model) {
                            $labelClass = 'success';
                            switch ($model->status) {
                                case Machine::STATUS_NOT_ACTIVE:
                                    $labelClass = 'danger';
                                    break;
                                case Machine::STATUS_ACTIVE:
                                    $labelClass = 'success';
                                    break;
                            }
                            return Html::tag('span', $model->getStatusName(),
                                ['class' => 'label label-' . $labelClass]);
                        }
                    ],

                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, Machine $model) {
                                return Html::a('Подробнее',
                                    ['view', 'id' => $model->id], ['class' => 'btn btn-primary']);
                            },
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?php Pjax::end(); ?>
</div>
