<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\Inspection */

$this->title = 'Провести осмотр';
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->machine->name, 'url' => ['/machine/view', 'id' => $model->machine_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inspection-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
