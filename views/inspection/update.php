<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\Inspection */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = [
    'label' => $model->getName(),
    'url' => ['/inspection/view', 'id' => $model->id]
];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="inspection-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
