<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use app\models\data\Inspection;
use yii\helpers\ArrayHelper;
use app\models\data\User;
use zxbodya\yii2\galleryManager\GalleryManager;

/* @var $this yii\web\View */
/* @var $model app\models\data\Inspection */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-default">
    <div class="box-body">
        <div class="inspection-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'status')->widget(Select2::class,
                        ['data' => Inspection::getStatusList()]) ?>
                    <?= $form->field($model, 'user_id')->widget(Select2::class, [
                        'data' => ArrayHelper::map(User::getUserListByRole(User::ROLE_ENGINEER), 'id',
                            'profile.nameWithPosition'),
                        'options' => ['placeholder' => 'Выберите ответственного за осмотры'],
                    ]) ?>
                    <?= $form->field($model, 'is_problem_found')->widget(Select2::class,
                        ['data' => [0 => 'Нет', 1 => 'Да']]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'notes')->textarea(['rows' => 6]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?php if ($model->isNewRecord): ?>
                            <p>Вы сможете прикрепить фотографии после сохранения.</p>
                        <?php else: ?>
                            <label>Фотографии</label>
                            <?= GalleryManager::widget([
                                'model' => $model,
                                'behaviorName' => 'galleryBehavior',
                                'apiRoute' => 'work/gallery-api'
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
