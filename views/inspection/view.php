<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\data\Inspection;
use app\models\data\User;

/* @var $this yii\web\View */
/* @var $model app\models\data\Inspection */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = ['label' => 'Оборудование', 'url' => ['index']];
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$images = $model->getBehavior('galleryBehavior')->getImages();
?>
<div class="inspection-view">

    <p>
        <?php if (Yii::$app->user->can(User::ROLE_ENGINEER)): ?>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php endif; ?>
    </p>

    <div class="box box-default">
        <div class="box-body">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function (Inspection $model) {
                            $labelClass = $model->status ? 'primary' : 'warning';
                            return Html::tag('span', $model->getStatusName(),
                                ['class' => 'label label-' . $labelClass]);
                        }
                    ],
                    'machine.name',
                    [
                        'attribute' => 'user_id',
                        'label' => 'Исполнитель',
                        'value' => function (Inspection $model) {
                            return $model->user->profile->name;
                        }
                    ],
                    'created_at:datetime',
                    [
                        'attribute' => 'is_problem_found',
                        'format' => 'raw',
                        'value' => function (Inspection $model) {
                            $labelClass = $model->is_problem_found ? 'danger' : 'success';
                            return Html::tag('span', Yii::$app->formatter->asBoolean($model->is_problem_found),
                                ['class' => 'label label-' . $labelClass]);
                        }
                    ],
                    'notes',
                ],
            ]) ?>
        </div>
    </div>

    <?php if (!empty($images)): ?>
        <h3>Фотографии</h3>
        <div class="box box-default">
            <div class="box-body">
                <div class="popup-gallery">
                    <?php foreach ($images as $image): ?>
                        <?= Html::a(Html::img($image->getUrl('preview'), ['width' => 100, 'height' => 100, 'style' => 'object-fit: cover;']),
                            $image->getUrl('medium'), ['title' => $image->description]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
