<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\data\PlannedWorkTask;
use app\models\data\User;

/* @var $this yii\web\View */
/* @var $model app\models\data\PlannedWork */
/* @var $plannedWorkTaskSearchModel \app\models\search\PlannedWorkSearch */
/* @var $plannedWorkTaskDataProvider \yii\data\ActiveDataProvider */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="planned-work-view">

    <p>
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Вы уверены?',
                    'method' => 'post',
                ],
            ]) ?>
        <?php endif; ?>
    </p>

    <div class="box box-default">
        <div class="box-body">
            <div class="table-responsive">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'frequencyStr',
                        'notes',
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <hr>
    <h3>Задачи</h3>
    <p>
        <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
            <?= Html::a('Добавить задачу', ['/planned-work-task/create', 'planned_work_id' => $model->id],
                ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
    </p>

    <?php
    $plannedWorkTaskGridViewButtonsTemplate = '';
    if (Yii::$app->user->can(User::ROLE_ADMIN)) {
        $plannedWorkTaskGridViewButtonsTemplate .= ' {update} {delete}';
    }
    ?>

    <div class="box box-default">
        <div class="box-body">
            <?= GridView::widget([
                'dataProvider' => $plannedWorkTaskDataProvider,
//            'filterModel' => $plannedWorkTaskSearchModel,
                'options' => [
                    'class' => 'table-responsive',
                ],
                'columns' => [
                    'name',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => $plannedWorkTaskGridViewButtonsTemplate,
                        'buttons' => [
                            'update' => function ($url, PlannedWorkTask $model) {
                                return Html::a('Редактировать',
                                    ['/planned-work-task/update', 'id' => $model->id], ['class' => 'btn btn-primary']);
                            },
                            'delete' => function ($url, PlannedWorkTask $model) {
                                return Html::a('Удалить',
                                    ['/planned-work-task/delete', 'id' => $model->id], [
                                        'data' => [
                                            'confirm' => 'Вы уверены?',
                                            'method' => 'post',
                                        ],
                                        'class' => 'btn btn-danger'
                                    ]);
                            }
                        ]
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
