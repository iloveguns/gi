<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\PlannedWork */

$this->title = 'Добавить плановое ТО';
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="planned-work-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
