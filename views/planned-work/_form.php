<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\data\Machine;

/* @var $this yii\web\View */
/* @var $model app\models\data\PlannedWork */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-default">
    <div class="box-body">
        <div class="planned-work-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-6">
                    <?php if ($model->machine->work_type == Machine::WORK_TYPE_DETAILS_COUNT): ?>
                        <?= $form->field($model, 'product_frequency')->textInput() ?>
                    <?php elseif ($model->machine->work_type == Machine::WORK_TYPE_PERIODS): ?>
                        <?= $form->field($model, 'day_frequency')->textInput() ?>
                    <?php endif; ?>

                    <?= $form->field($model, 'notes')->textarea(['rows' => 6, 'maxlength' => true]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
