<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\PlannedWorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Planned Works';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-default">
    <div class="box-body">
        <div class="planned-work-index">

            <h1><?= Html::encode($this->title) ?></h1>
            <?php Pjax::begin(); ?>
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

            <p>
                <?= Html::a('Create Planned Work', ['create'], ['class' => 'btn btn-success']) ?>
            </p>

            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'options' => [
                    'class' => 'table-responsive',
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id',
                    'machine_id',
                    'product_frequency',
                    'day_frequency',
                    'notes',
                    //'created_at',
                    //'updated_at',
                    //'created_by',
                    //'updated_by',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

