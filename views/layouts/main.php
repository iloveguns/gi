<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\data\User;
use app\models\data\Task;

AppAsset::register($this);

/**
 * @var $currAuthUserModel \app\models\data\User
 */
$currAuthUserModel = Yii::$app->user->identity;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link rel="shortcut icon" href="/landing/images/logo-blu.png" type="image/png">
    <?php $this->head() ?>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<?php $this->beginBody() ?>


<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>ГИ</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b><?= Yii::$app->name ?></b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button"><i class="fas fa-bars"></i></a>
            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
                    <?php if (Yii::$app->user->can(User::ROLE_ADMIN)): ?>
                        <li>
                            <a href="javascript:void(0);">Баланс: <?= Yii::$app->formatter->asCurrency(\app\models\data\Transaction::getBalance()) ?></a>
                        </li>
                    <?php endif; ?>
                    <?php /*echo \webzop\notifications\widgets\Notifications::widget([
                        'clientOptions' => [
                            'readLabel' => 'Прочитано',
                            'markAsReadLabel' => 'Новое'
                        ]
                    ])*/ ?>
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i>
                            <span class="hidden-xs"><?= $currAuthUserModel->profile->name ?></span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-right">
                                    <?= Html::a('Профиль', ['/user/settings/profile'],
                                        ['class' => 'btn btn-default btn-flat']) ?>
                                    <?= Html::a('Выйти', ['/user/security/logout'],
                                        ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']) ?>
                                </div>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu">
                <li><?= Html::a('<i class="fas fa-toolbox"></i> Оборудование', ['/machine']) ?></li>

                <?php if (Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
                    <li><?= Html::a('<i class="fas fa-tasks"></i> Текущие задачи', [
                            '/task/index',
                            'TaskSearch' => [
                                'status' => [Task::STATUS_NOT_COMPLETED, Task::STATUS_PROCESS, Task::STATUS_TEST, Task::STATUS_REJECTED]
                            ]
                        ]) ?></li>
                    <li><?= Html::a('<i class="fas fa-stethoscope"></i> Техническое обслуживание',
                            ['/work/index']) ?></li>
                    <li><?= Html::a('<i class="fas fa-wrench"></i> Задания', ['/repair-task/index']) ?></li>
                <?php elseif (Yii::$app->user->can(User::ROLE_ENGINEER)): ?>
                    <li><?= Html::a('<i class="fas fa-tasks"></i> Текущие задачи',
                            [
                                '/task/index',
                                'TaskSearch' => [
                                    'user_id' => Yii::$app->user->id,
                                    'status' => [Task::STATUS_NOT_COMPLETED, Task::STATUS_PROCESS, Task::STATUS_REJECTED]
                                ]
                            ]) ?></li>
                    <li><?= Html::a('<i class="fas fa-stethoscope"></i> Техническое обслуживание', ['/work/index']) ?></li>
                    <li><?= Html::a('<i class="fas fa-wrench"></i> Задания', ['/repair-task/index']) ?></li>
                <?php endif; ?>

                <?php if ($currAuthUserModel->isAdmin): ?>
                    <li><?= Html::a('<i class="fa fa-users"></i> Пользователи', ['/user/admin/index']) ?></li>
                    <li><?= Html::a('<i class="fas fa-ruble-sign"></i> Транзакции', ['/transaction/index']) ?></li>
                <?php endif; ?>
            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1><?= $this->title ?></h1>
            <?= Breadcrumbs::widget([
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]) ?>
        </section>

        <!-- Main content -->
        <section class="content">
            <?= Alert::widget() ?>
            <?= $content ?>

        </section><!-- /.content -->
    </div><!-- /.content-wrapper -->

    <footer class="main-footer">
        <strong>&copy; <?= date('Y') ?> <?= Yii::$app->name ?></strong> Все права защищены.
    </footer>
</div><!-- ./wrapper -->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
