<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use app\models\data\Machine;
use app\models\data\Work;
use app\models\data\User;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\WorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Техническое обслуживание';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="box box-default">
    <div class="box-body">
        <?php Pjax::begin(); ?>
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

        <?php $columns = []; ?>
        <?php if (Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
            <?php $columns[] = [
                'attribute' => 'work_user_id',
                'value' => 'workUser.profile.name',
                'filter' => Select2::widget([
                    'model' => $searchModel,
                    'attribute' => 'work_user_id',
                    'data' => ArrayHelper::map(User::getUserListByRole([User::ROLE_ENGINEER]), 'id',
                        'profile.nameWithPosition'),
                    'options' => ['prompt' => ''],
                    'pluginOptions' => ['allowClear' => true]
                ])
            ]; ?>
        <?php endif; ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'options' => [
                'class' => 'table-responsive',
            ],
            'columns' => ArrayHelper::merge($columns, [
                [
                    'attribute' => 'machine_id',
                    'format' => 'raw',
                    'value' => function (Work $model) {
                        return Html::a($model->machine->name, $model->machine->getUrl(), ['target' => '_blank']);
                    },
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'machine_id',
                        'data' => ArrayHelper::map(Machine::find()->all(), 'id', 'name'),
                        'options' => ['prompt' => ''],
                        'pluginOptions' => ['allowClear' => true]
                    ])
                ],
                [
                    'attribute' => 'planned_ended_at',
                    'format' => 'datetime',
                    'filter' => false,
                ],
                    [
                        'attribute' => 'started_at',
                        'format' => 'datetime',
                        'filter' => false,
                    ],
                [
                    'attribute' => 'type',
                    'value' => 'typeName',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'type',
                        'data' => Work::getTypeList(),
                        'options' => ['prompt' => ''],
                        'pluginOptions' => ['allowClear' => true]
                    ])
                ],
                [
                    'attribute' => 'status',
                    'format' => 'raw',
                    'value' => 'statusLabel',
                    'filter' => Select2::widget([
                        'model' => $searchModel,
                        'attribute' => 'status',
                        'data' => Work::getStatusList(),
                        'options' => ['prompt' => '', 'multiple' => true],
                        'pluginOptions' => ['allowClear' => true]
                    ]),
                ],
                [
                    'attribute' => 'ended_at',
                    'format' => 'datetime',
                    'filter' => false,
                ],
                [
                    'class' => 'yii\grid\ActionColumn',
                    'template' => '{view}',
                    'buttons' => [
                        'view' => function ($url, Work $model) {
                            return Html::a('Подробнее',
                                ['view', 'id' => $model->id], ['class' => 'btn btn-primary']);
                        },
                    ]
                ]
            ]),
        ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>
