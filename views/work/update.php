<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\Work */

$this->title = $model->getTypeName();
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
