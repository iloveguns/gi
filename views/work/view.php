<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use app\models\data\Work;
use app\models\data\WorkTask;
use app\models\data\User;

/* @var $this yii\web\View */
/* @var $model app\models\data\Work */
/* @var $workTaskSearchModel \app\models\search\WorkTaskSearch */
/* @var $workTaskDataProvider yii\data\ActiveDataProvider */

$this->title = $model->getTypeName();
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$images = $model->getBehavior('galleryBehavior')->getImages();
?>
<div class="work-view">

    <p>
        <?php if ($model->status != Work::STATUS_COMPLETED): ?>
            <?php if (Yii::$app->user->can(User::ROLE_ENGINEER) || Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
                <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ($model->status == Work::STATUS_CREATED): ?>
            <?php if (Yii::$app->user->id == $model->work_user_id): ?>
                <?= Html::a('Начать работу', ['change-status', 'id' => $model->id, 'status' => Work::STATUS_IN_PROCESS],
                    ['class' => 'btn btn-success']) ?>
            <?php endif; ?>
        <?php elseif ($model->status == Work::STATUS_REJECTED): ?>
            <?php if (Yii::$app->user->id == $model->work_user_id): ?>
                <?= Html::a('Начать работу', ['change-status', 'id' => $model->id, 'status' => Work::STATUS_IN_PROCESS],
                    ['class' => 'btn btn-success']) ?>
            <?php endif; ?>
        <?php elseif ($model->status == Work::STATUS_IN_PROCESS): ?>
            <?php if (Yii::$app->user->id == $model->work_user_id && $model->isCompleted()): ?>
                <?= Html::a('Работа выполнена',
                    ['change-status', 'id' => $model->id, 'status' => Work::STATUS_TEST],
                    ['class' => 'btn btn-warning']) ?>
            <?php endif; ?>
        <?php elseif ($model->status == Work::STATUS_TEST): ?>
            <?php if (Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
                <?= Html::a('Принять работу',
                    ['change-status', 'id' => $model->id, 'status' => Work::STATUS_COMPLETED],
                    ['class' => 'btn btn-info']) ?>
                <?= Html::a('Отклонить работу',
                    ['change-status', 'id' => $model->id, 'status' => Work::STATUS_REJECTED],
                    ['class' => 'btn btn-danger']) ?>
            <?php endif; ?>
        <?php endif; ?>
    </p>

    <div class="box box-default">
        <div class="box-body">
            <div class="table-responsive">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'machine.name',
                            'label' => 'Оборудование'
                        ],
                        [
                            'attribute' => 'work_user_id',
                            'label' => 'Исполнитель',
                            'value' => function (Work $model) {
                                return $model->workUser->profile->name;
                            },
                        ],
                        'planned_ended_at:datetime',
                        'started_at:datetime',
                        'ended_at:datetime',
                        'checked_at:datetime',
                        'notes',
                        [
                            'attribute' => 'statusName',
                            'format' => 'raw',
                            'value' => function (Work $model) {
                                $labelClass = 'success';
                                switch ($model->status) {
                                    case Work::STATUS_CREATED:
                                        $labelClass = 'danger';
                                        break;
                                    case Work::STATUS_IN_PROCESS:
                                        $labelClass = 'primary';
                                        break;
                                    case Work::STATUS_TEST:
                                        $labelClass = 'warning';
                                        break;
                                    case Work::STATUS_COMPLETED:
                                        $labelClass = 'success';
                                        break;
                                    case Work::STATUS_REJECTED:
                                        $labelClass = 'danger';
                                        break;
                                }

                                return Html::tag('span', $model->getStatusName(),
                                    ['class' => 'label label-' . $labelClass]);
                            },
                        ],
                    ],
                ]) ?>
            </div>
        </div>
    </div>

    <?php if (!empty($images)): ?>
        <h3>Фотографии</h3>
        <div class="box box-default">
            <div class="box-body">
                <div class="popup-gallery">
                    <?php foreach ($images as $image): ?>
                        <?= Html::a(Html::img($image->getUrl('preview'),
                            ['width' => 100, 'height' => 100, 'style' => 'object-fit: cover;']),
                            $image->getUrl('medium'), ['title' => $image->description]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($model->type == Work::TYPE_PLANNED): ?>
        <h3>Задачи к выполнению</h3>

        <div class="box box-default">
            <div class="box-body">
                <?= GridView::widget([
                    'dataProvider' => $workTaskDataProvider,
//            'filterModel' => $workTaskSearchModel,
                    'options' => [
                        'class' => 'table-responsive',
                    ],
                    'columns' => [
                        'plannedWorkTask.name',
                        'statusName',
                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{change-status}',
                            'buttons' => [
                                'change-status' => function ($url, WorkTask $model) {
                                    return $model->status == WorkTask::STATUS_NOT_COMPLETED ? Html::a('Выполнено',
                                        [
                                            '/work-task/change-status',
                                            'id' => $model->id,
                                            'status' => WorkTask::STATUS_COMPLETED
                                        ], ['class' => 'btn btn-warning']) : '';
                                },
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    <?php endif; ?>
</div>
