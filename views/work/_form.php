<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\data\Work;
use yii\helpers\ArrayHelper;
use app\models\data\PlannedWork;
use kartik\datetime\DateTimePicker;
use app\models\data\Machine;
use app\models\data\User;
use kartik\select2\Select2;
use zxbodya\yii2\galleryManager\GalleryManager;

/* @var $this yii\web\View */
/* @var $model app\models\data\Work */
/* @var $form yii\widgets\ActiveForm */

$workUserRole = $model->machine->work_type == Machine::TYPE_ELECTRICAL ? User::ROLE_ELECTRIC : User::ROLE_MECHANIC;
?>

<div class="box box-default">
    <div class="box-body">
        <div class="work-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'type')->dropDownList(Work::getTypeList()) ?>

                    <div class="js-planned-work-container">
                        <?= $form->field($model,
                            'planned_work_id')
                            ->widget(Select2::class, [
                                'data' => ArrayHelper::map(PlannedWork::findAll(['machine_id' => $model->machine_id]),
                                    'id',
                                    'name'),
                                'options' => ['placeholder' => 'Выберите плановое ТО'],
                            ]) ?>
                    </div>

                    <?php if (Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
                        <?= $form->field($model,
                            'work_user_id')
                            ->widget(Select2::class, [
                                'data' => ArrayHelper::map(User::getUserListByRole($workUserRole), 'id',
                                    'profile.nameWithPosition'),
                                'options' => ['placeholder' => 'Выберите исполнителя'],
                            ]) ?>
                    <?php endif; ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'plannedEndedAtStr')->widget(DateTimePicker::class, [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy hh:ii'
                        ]
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'status')->dropDownList(Work::getStatusList()) ?>
                    <?= $form->field($model, 'notes')->textarea(['rows' => 6, 'maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?php if ($model->isNewRecord): ?>
                            <p>Вы сможете прикрепить фотографии после сохранения.</p>
                        <?php else: ?>
                            <label>Фотографии</label>
                            <?= GalleryManager::widget([
                                'model' => $model,
                                'behaviorName' => 'galleryBehavior',
                                'apiRoute' => 'work/gallery-api'
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>

<?php $this->registerJs('
        var $workTypeSelect = $("#work-type");
        var $plannedWork = $("#work-planned_work_id");
        var $plannedWorkContainer = $(".js-planned-work-container");
        
        function workTypeChange(){
            var valueSelected = $workTypeSelect.val();
            if (valueSelected == "' . Work::TYPE_NOT_PLANNED . '") {
                $plannedWork.val("").trigger("change");
                $plannedWorkContainer.hide();
            } else if (valueSelected == "' . Work::TYPE_PLANNED . '") {
                $plannedWorkContainer.show();
            }
        } workTypeChange();
        
        $workTypeSelect.on("change", function () {
            workTypeChange();
        });
        ', \yii\web\View::POS_READY) ?>

