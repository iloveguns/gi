<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\data\User;
use app\models\data\Examination;

/* @var $this yii\web\View */
/* @var $model app\models\data\Examination */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="box box-default">
    <div class="box-body">
        <div class="examination-view">

            <p>
                <?php if ($model->status == Examination::STATUS_NOT_COMPLETED && (Yii::$app->user->can(User::ROLE_ADMIN) || Yii::$app->user->id == $model->machine->responsible_user_id)): ?>
                    <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?php endif; ?>
            </p>

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'document_number',
                    'machine.name',
                    'date:date',
                    'notify_in_days',
                    'notes',
                    [
                        'attribute' => 'statusName',
                        'format' => 'raw',
                        'value' => function (Examination $model) {
                            $labelClass = $model->status ? 'success' : 'danger';
                            return Html::tag('span', $model->getStatusName(),
                                ['class' => 'label label-' . $labelClass]);
                        },
                    ],
                ],
            ]) ?>

        </div>
    </div>
</div>
