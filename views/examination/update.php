<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\Examination */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examination-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
