<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\data\Examination;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\data\Examination */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="box box-default">
    <div class="box-body">
        <div class="examination-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'document_number')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'dateStr')->widget(DatePicker::class, [
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                            'todayHighlight' => true,
                            'endDate' => 'current',
                        ],
                    ]) ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'status')->dropDownList(Examination::getStatusList()) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'notes')->textarea(['rows' => 6, 'maxlength' => true]) ?>
                </div>

                <div class="col-md-4">
                    <?= $form->field($model, 'nextDateStr')->widget(DatePicker::class, [
                        'type' => DatePicker::TYPE_COMPONENT_APPEND,
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy',
                        ],
                        'options' => [
                            'required' => 'required'
                        ]
                    ]) ?>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
