<!DOCTYPE html>
<html lang="ru">
<head>
    <!-- Encoding Meta -->
    <meta charset="UTF-8"/>
    <!-- IE Compatibility Meta -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <!-- Meta Viewport -->
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- Description -->
    <meta name="description" content="ERM-система для контроля за оборудованием на производственном предприятии"/>

    <!-- Title -->
    <title><?= Yii::$app->name ?></title>

    <!-- Poppins Font -->
    <link href="https://fonts.googleapis.com/css?family=Ubuntu:300,400,500,700&amp;subset=cyrillic,cyrillic-ext,latin-ext"
          rel="stylesheet">

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/landing/css/bootstrap.min.css"/>
    <!-- Font-Awesome -->
    <link rel="stylesheet" href="/landing/css/font-awesome.min.css"/>
    <!-- Et-Line -->
    <link rel="stylesheet" href="/landing/css/et-line.min.css"/>
    <!-- Owl-Carousel -->
    <link rel="stylesheet" href="/landing/css/owl.carousel.min.css"/>
    <!-- Animate -->
    <link rel="stylesheet" href="/landing/css/animate.min.css"/>
    <!-- Magnific-PopUp -->
    <link rel="stylesheet" href="/landing/css/magnific-popup.min.css"/>
    <!-- Main Style -->
    <link rel="stylesheet" href="/landing/css/style.css"/>
    <!-- New Style -->
    <link rel="stylesheet" href="/landing/css/new_style.css"/>

    <link rel="shortcut icon" href="/landing/images/logo-blu.png" type="image/png">
</head>
<body data-spy="scroll" data-target=".navbar" data-offset="50" data-default-theme="theme-20">
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (m, e, t, r, i, k, a) {
        m[i] = m[i] || function () {
            (m[i].a = m[i].a || []).push(arguments)
        };
        m[i].l = 1 * new Date();
        k = e.createElement(t), a = e.getElementsByTagName(t)[0], k.async = 1, k.src = r, a.parentNode.insertBefore(k, a)
    })
    (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

    ym(53043121, "init", {
        clickmap: true,
        trackLinks: true,
        accurateTrackBounce: true,
        webvisor: true
    });
</script>
<noscript>
    <div><img src="https://mc.yandex.ru/watch/53043121" style="position:absolute; left:-9999px;" alt=""/></div>
</noscript>
<!-- /Yandex.Metrika counter -->

<!-- Start Loading -->
<div class="loading">
    <div class="spinner">
        <div class="cube1"></div>
        <div class="cube2 color-1-bg"></div>
    </div>
</div>
<!-- End Loading -->
<!-- Start ScrollToTop -->
<i class="fa fa-angle-up scroll color-1-bg"></i>
<!-- End ScrollToTop -->
<!-- Start Colors Switcher -->
<?php /*
<div class="switcher">
    <h4>choose gradients</h4>
    <div class="gradients">
        <span data-theme="theme-1" class="active"></span>
        <span data-theme="theme-2"></span>
        <span data-theme="theme-3"></span>
        <span data-theme="theme-4"></span>
        <span data-theme="theme-5"></span>
        <span data-theme="theme-6"></span>
        <span data-theme="theme-7"></span>
        <span data-theme="theme-8"></span>
        <span data-theme="theme-9"></span>
        <span data-theme="theme-10"></span>
        <span data-theme="theme-11"></span>
        <span data-theme="theme-12"></span>
        <span data-theme="theme-13"></span>
        <span data-theme="theme-14"></span>
        <span data-theme="theme-15"></span>
        <span data-theme="theme-16"></span>
        <span data-theme="theme-17"></span>
        <span data-theme="theme-18"></span>
        <span data-theme="theme-19"></span>
        <span data-theme="theme-20"></span>
    </div>
    <div class="cog">
        <i class="fa fa-cog"></i>
    </div>
</div>
*/ ?>
<!-- Start Colors Switcher -->
<!-- Start Navbar -->

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false">
                <span class="sr-only">Навигация</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img src="/landing/images/logo.png" alt="">
                <span class="navbar-brand-name"><?= Yii::$app->name ?></span>
            </a>
        </div>

        <div class="collapse navbar-collapse" id="navbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#for-what">О системе</a></li>
                <li><a href="#features">Преимущества</a></li>
                <li><a href="#support">Поддержка</a></li>
                <li><a href="#contact">Контакты</a></li>
            </ul>
        </div>
    </div>
</nav>

<!-- End Navbar -->
<!-- Start Hero -->
<section id="hero">
    <div class="overlay gradient"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="contain">
                    <div class="box">
                        <h1><?= Yii::$app->name ?></h1>
                        <p>ERM-система для контроля за оборудованием на производственном предприятии</p>
                        <a href="#" class="btn download color-1-hover js-join" onclick="yaCounter53043121.reachGoal('joinbutton'); return true;"><i class="fa fa-sign-in"></i>Присоединиться</a>
                        <a href="/user/login" target="_blank" class="btn download color-1-hover" onclick="yaCounter53043121.reachGoal('demobutton1'); return true;"><i
                                    class="fa fa-play"></i>Демо</a>
                        <!--                        <a href="https://www.youtube.com/watch?v=JQr-agnUaEM" class="btn video color-1-hover"><i-->
                        <!--                                    class="fa fa-play"></i>Демо</a>-->
                    </div>
                </div>
            </div>
            <div class="col-md-6 hidden-sm hidden-xs">
                <div class="box">
                    <img src="/landing/images/app1.png" alt="app"/>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Hero -->
<!-- Start Services -->
<div id="for-what" class="services p100 text-center">
    <div class="container">
        <div class="title">
            <i class="icon-lightbulb color-1"></i>
            <h2>Для чего? <span class="bg-text">Информатизация</span></h2>
            <p>ERM-система «<?= Yii::$app->name ?>» позволяет решить важную проблему на производственном предприятии –
                организация и контроль за состоянием и техническим обслуживанием оборудования.</p>
        </div>
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="box">
                    <i class="icon-tools-2 color-1"></i>
                    <h3>Полный контроль</h3>
                    <p>Полный контроль за проведением ТО и ремонта на всех этапах работ</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="box">
                    <i class="icon-calendar color-1"></i>
                    <h3>Планирование</h3>
                    <p>Возможность планирования технического обслуживания и ремонта оборудования</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="box">
                    <i class="icon-chat color-1"></i>
                    <h3>Напоминания</h3>
                    <p>Ежедневные напоминания о предстоящих задачах</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="box">
                    <i class="icon-download color-1"></i>
                    <h3>Достоверность</h3>
                    <p>Полная достоверность и актуальность данных</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="box">
                    <i class="icon-presentation color-1"></i>
                    <h3>Анализ</h3>
                    <p>Наличие статистических данных для анализа и принятия решений</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="box">
                    <i class="icon-document color-1"></i>
                    <h3>Отчетность</h3>
                    <p>Наличие электронной отчётности о выполнении работ</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Services -->
<!-- Start About -->
<section class="about p100 gry" id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="box image">
                    <img src="/landing/images/app.png" alt="app"/>
                </div>
            </div>
            <div class="col-md-7">
                <div class="box content">
                    <h2>Современное <span class="color-2">решение</span></h2>
                    <p>Благодаря внедрению информационных технологий, контроль за большим количеством оборудования и
                        оснастки, требующей осмотра, обслуживания, ремонта и освидетельствования, становится значительно
                        проще.</p>
                    <p>Наличие системы определения состояния оборудования на основе имеющихся данных позволяет избежать
                        срыв сроков обслуживания и возникновения риска поломки в результате человеческого фактора –
                        «забыл сделать».</p>
                    <p>Информатизация бизнес-процесса обслуживания не требует коммуникации между обслуживающим и
                        эксплуатирующим персоналом, снижает уровень вероятности фальсификации данных по факту выполнения
                        работ, а также включает преемственность информации при ротации обслуживающего персонала.</p>
                    <a href="/user/login" target="_blank" class="btn download color-1-bg" onclick="yaCounter53043121.reachGoal('demobutton2'); return true;"><i class="fa fa-play"></i>
                        Попробовать демо</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End About -->
<!-- Start Features -->
<section class="features p100" id="features">
    <div class="container">
        <div class="title text-center">
            <i class="icon-layers color-1"></i>
            <h2>Преимущества использования <span class="bg-text">Функции</span></h2>
            <p>ERM-система «<?= Yii::$app->name ?>» – это веб-приложение, основой которого является программное ядро с
                минимально необходимым функционалом для качественного контроля за оборудованием.</p>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="items box text-right text-left--mobile">
                    <div class="item box">
                        <div class="item-icon">
                            <i class="icon-desktop color-2"></i>
                        </div>
                        <div class="item-content">
                            <h4>Простота использования</h4>
                            <p>Процесс обучения сотрудника работе в системе занимаете не более 1 часа.</p>
                        </div>
                    </div>
                    <div class="item box">
                        <div class="item-icon">
                            <i class="icon-adjustments color-2"></i>
                        </div>
                        <div class="item-content">
                            <h4>Гибкость</h4>
                            <p>Безопасно адаптируемо под любые бизнес-процессы, связанные с обслуживанием
                                оборудования.</p>
                        </div>
                    </div>
                    <div class="item box">
                        <div class="item-icon">
                            <i class="icon-calendar color-2"></i>
                        </div>
                        <div class="item-content">
                            <h4>Аналитика</h4>
                            <p>Дата ремонта или обслуживания автоматически формируется в зависимости от выбранного типа
                                регламента.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="image box">
                    <img src="/landing/images/app.png" alt="app"/>
                </div>
            </div>
            <div class="col-md-4">
                <div class="items box">
                    <div class="item box">
                        <div class="item-icon">
                            <i class="icon-lock color-2"></i>
                        </div>
                        <div class="item-content">
                            <h4>Безопасность</h4>
                            <p>Вся информация надежно зашифрована и хранится в облачном хранилище данных.</p>
                        </div>
                    </div>
                    <div class="item box">
                        <div class="item-icon">
                            <i class="icon-mobile color-2"></i>
                        </div>
                        <div class="item-content">
                            <h4>Удобство</h4>
                            <p>Система доступна для пользования в любом месте и на любом устройстве с наличием
                                подключения к
                                сети Интернет.</p>
                        </div>
                    </div>
                    <div class="item box">
                        <div class="item-icon">
                            <i class="icon-profile-male color-2"></i>
                        </div>
                        <div class="item-content">
                            <h4>Автоматизм</h4>
                            <p>Частичное избавление от зависимости возникновения неисправностей от человеческого
                                фактора.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End Features -->
<!-- Цифры, факты -->
<?php /*
<section class="fun-fact p100 text-center">
    <div class="overlay gradient"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-6">
                <div class="box">
                    <i class="icon-download"></i>
                    <h4 class="counter">9,125</h4>
                    <span>app download</span>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="box">
                    <i class="icon-genius"></i>
                    <h4 class="counter">68,700</h4>
                    <span>line of code</span>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="box">
                    <i class="icon-wine"></i>
                    <h4 class="counter">534</h4>
                    <span>coffee cups</span>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="box">
                    <i class="icon-linegraph"></i>
                    <h4 class="counter">4,485</h4>
                    <span>share app</span>
                </div>
            </div>
        </div>
    </div>
</section>
 */ ?>
<!-- Цифры, факты -->
<!-- Start One-Feature -->

<section id="support" class="one-feature p100 gry">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="box image">
                    <img src="/landing/images/tp.png" alt="one-feature"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box content">
                    <h5>Техническая поддержка</h5>
                    <p>На всем этапе внедрения и эксплуатации производится полная техническая поддержка системы
                        предприятия.
                        Использующие ее сотрудники будут обучены и проконсультированы по любым возникшим вопросам, что
                        исключает синдром «болезненного» внедрения, а также необходимости привлечения IT-отдела
                        предприятия.</p>
                    <a href="#" class="btn more color-1-bg js-join">Связаться<i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End One-Feature -->
<!-- Start One-Feature -->
<?php /*
<section class="one-feature p100">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="box content">
                    <h5>make business easy with tox.</h5>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut
                        labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
                        laboris nisi ut aliquip ex ea commodo consequat.</p>
                    <a href="#" class="btn more color-1-bg">read more<i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box image">
                    <img src="/landing/images/one-feature-2.png" alt="one-feature-2"/>
                </div>
            </div>
        </div>
    </div>
</section>
 */ ?>
<!-- End One-Feature -->

<!-- Start Screenshots -->
<?php /*
<section class="screenshots p100 text-center" id="screenshots">
    <div class="container">
        <div class="title">
            <i class="icon-pictures color-1"></i>
            <h2>app screenshots<span class="bg-text">screenshot</span></h2>
            <p>Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Sed Feugiat Arcu Ut Orci Porta, Eget Porttitor
                Felis Suscipit. Sed A Nisl.</p>
        </div>
        <div class="row owl-carousel screenshots-carousel">
            <div class="box">
                <img src="/landing/images/screenshots/01.jpg" alt="screen-1"/>
            </div>
            <div class="box">
                <img src="/landing/images/screenshots/02.jpg" alt="screen-2"/>
            </div>
            <div class="box">
                <img src="/landing/images/screenshots/03.jpg" alt="screen-3"/>
            </div>
            <div class="box">
                <img src="/landing/images/screenshots/04.jpg" alt="screen-4"/>
            </div>
            <div class="box">
                <img src="/landing/images/screenshots/05.jpg" alt="screen-5"/>
            </div>
            <div class="box">
                <img src="/landing/images/screenshots/06.jpg" alt="screen-6"/>
            </div>
            <div class="box">
                <img src="/landing/images/screenshots/07.jpg" alt="screen-7"/>
            </div>
            <div class="box">
                <img src="/landing/images/screenshots/01.jpg" alt="screen-1"/>
            </div>
            <div class="box">
                <img src="/landing/images/screenshots/05.jpg" alt="screen-5"/>
            </div>
        </div>
    </div>
</section>
 */ ?>
<!-- End Screenshots -->
<!-- Start Download -->
<?php /*
<section class="download-section p100 text-center">
    <div class="overlay gradient"></div>
    <div class="container">
        <div class="title">
            <h2>download <span class="bg-text">download</span></h2>
            <p>Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Sed Feugiat Arcu Ut Orci Porta, Eget Porttitor
                Felis Suscipit. Sed A Nisl.</p>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="box">
                    <a href="#" class="btn d-all color-1-hover"><i class="fa fa-apple"></i> app store</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <a href="#" class="btn d-all color-1-hover"><i class="fa fa-android"></i> google play</a>
                </div>
            </div>
            <div class="col-md-4">
                <div class="box">
                    <a href="#" class="btn d-all color-1-hover"><i class="fa fa-windows"></i> windows store</a>
                </div>
            </div>
        </div>
    </div>
</section>
 */ ?>
<!-- End Download -->
<!-- Start Pricing -->
<?php /*
<section class="pricing gry p100 text-center" id="pricing">
    <div class="container">
        <div class="title">
            <i class="icon-wallet color-1"></i>
            <h2>Стоимость <span class="bg-text">Стоимость</span></h2>
            <p>Стоимость внедрения может меняться в зависимости от сложности бизнес-процессов на предприятии.</p>
        </div>
        <div class="row">

            <div class="col-md-4">
                <div class="box">
                    <div class="item-head">
                        <h3>basic</h3>
                        <span class="color-1">single user license</span>
                    </div>
                    <div class="item-price">
                        <h4 class="color-2">$4.99 <span>per month</span></h4>
                    </div>
                    <div class="item-features">
                        <ul class="list-unstyled">
                            <li>1 GB Of Space</li>
                            <li>Real-time sync</li>
                            <li>Unlimited Attachments</li>
                            <li>Customize Themes</li>
                            <li>Priority email support</li>
                        </ul>
                    </div>
                    <div class="item-buy">
                        <a href="#" class="btn buy color-2-bg">buy now</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-md-offset-4">
                <div class="box"> <!-- center-box -->
                    <div class="item-head">
                        <h3>150 000 ₽</h3>
                        <span class="color-1">разовая стоимость внедрения системы</span>
                    </div>
                    <div class="item-features">
                        <ul class="list-unstyled">
                            <li>далее 150 ₽ в год за одну единицу оборудования</li>
                        </ul>
                    </div>
                    <?php /*<div class="item-buy">
                        <a href="#" class="btn buy color-2-bg">buy now</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
                <div class="box">
                    <div class="item-head">
                        <h3>premium</h3>
                        <span class="color-1">multi user license</span>
                    </div>
                    <div class="item-price">
                        <h4 class="color-2">$19.99 <span>per month</span></h4>
                    </div>
                    <div class="item-features">
                        <ul class="list-unstyled">
                            <li>3 GB Of Space</li>
                            <li>Real-time sync</li>
                            <li>Unlimited Attachments</li>
                            <li>Customize Themes</li>
                            <li>Priority email support</li>
                        </ul>
                    </div>
                    <div class="item-buy">
                        <a href="#" class="btn buy color-2-bg">buy now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
*/ ?>
<!-- End Pricing -->
<!-- Start Testimonials -->
<?php /*
<section class="testimonials p100 text-center">
    <div class="overlay gradient"></div>
    <div class="container">
        <div class="row owl-carousel testimonials-carousel">
            <div class="box">
                <img src="/landing/images/testimonials/p1.jpeg" alt="p1" class="img-circle img-thumbnail"/>
                <h4>mostafa sobhy</h4>
                <ul class="list-unstyled">
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio provident dolores perspiciatis. Dicta
                    temporibus nemo, nam explicabo nihil repellendus laboriosam autem porro est consequuntur, veniam
                    error sit perferendis, iste ex.</p>
            </div>
            <div class="box">
                <img src="/landing/images/testimonials/p2.jpg" alt="p1" class="img-circle img-thumbnail"/>
                <h4>abdo nazem</h4>
                <ul class="list-unstyled">
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio provident dolores perspiciatis. Dicta
                    temporibus nemo, nam explicabo nihil repellendus laboriosam autem porro est consequuntur, veniam
                    error sit perferendis, iste ex.</p>
            </div>
            <div class="box">
                <img src="/landing/images/testimonials/p3.jpeg" alt="p1" class="img-circle img-thumbnail"/>
                <h4>ahmed mokhtar</h4>
                <ul class="list-unstyled">
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                    <li><i class="fa fa-star"></i></li>
                </ul>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Optio provident dolores perspiciatis. Dicta
                    temporibus nemo, nam explicabo nihil repellendus laboriosam autem porro est consequuntur, veniam
                    error sit perferendis, iste ex.</p>
            </div>
        </div>
    </div>
</section>
 */ ?>
<!-- End Testimonials -->
<!-- Start Team -->
<?php /*
<section class="team p100" id="team">
    <div class="container">
        <div class="title text-center">
            <i class="icon-profile-male color-1"></i>
            <h2>our team <span class="bg-text">team</span></h2>
            <p>Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Sed Feugiat Arcu Ut Orci Porta, Eget Porttitor
                Felis Suscipit. Sed A Nisl.</p>
        </div>
        <div class="row owl-carousel team-carousel">
            <div class="box">
                <img src="/landing/images/team/team-1.jpeg" alt="team-1"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>mostafa sobhy</h4>
                        <span>front-end</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/team/team-2.jpeg" alt="team-2"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>hamza nabil</h4>
                        <span>back-end</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/team/team-3.jpeg" alt="team-3"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>ali ibrahim</h4>
                        <span>ceo founder</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/team/team-4.jpeg" alt="team-4"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>mazen ahmed</h4>
                        <span>graphic designer</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/team/team-1.jpeg" alt="team-1"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>mostafa sobhy</h4>
                        <span>front-end</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/team/team-2.jpeg" alt="team-2"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>hamza nabil</h4>
                        <span>back-end</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/team/team-3.jpeg" alt="team-3"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>ali ibrahim</h4>
                        <span>ceo founder</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/team/team-4.jpeg" alt="team-4"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>mazen ahmed</h4>
                        <span>graphic designer</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/team/team-1.jpeg" alt="team-1"/>
                <div class="overlay gradient">
                    <div class="content">
                        <h4>mostafa sobhy</h4>
                        <span>front-end</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                        <div class="icons">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-google-plus"></i></a>
                            <a href="#"><i class="fa fa-linkedin"></i></a>
                            <a href="#"><i class="fa fa-behance"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
 */ ?>
<!-- End Team -->
<!-- Start Blog -->
<?php /*
<section class="blog gry p100" id="blog">
    <div class="container">
        <div class="title text-center">
            <i class="icon-global color-1"></i>
            <h2>blog <span class="bg-text">news</span></h2>
            <p>Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Sed Feugiat Arcu Ut Orci Porta, Eget Porttitor
                Felis Suscipit. Sed A Nisl.</p>
        </div>
        <div class="row owl-carousel blog-carousel">
            <div class="box">
                <img src="/landing/images/blog/1.jpg" alt="b1"/>
                <div class="content">
                    <h4><a href="http://sob7y.ml/single-blog">easy with app use want to</a></h4>
                    <div class="info">
                        <span><i class="fa fa-user"></i><a href="#" class="color-2">by: tiex</a></span>
                        <time datetime="2017-04-17"><i class="fa fa-calendar"></i>17 April, 2017</time>
                        <span><i class="fa fa-comment"></i>5 comments</span>
                    </div>
                    <p>In this article, I’d like to reacquaint you do with the humbler work of communication that is the
                        paragraph.</p>
                    <a href="http://sob7y.ml/single-blog" class="read">read more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/blog/2.jpg" alt="b2"/>
                <div class="content">
                    <h4><a href="http://sob7y.ml/single-blog">easy with app use want to</a></h4>
                    <div class="info">
                        <span><i class="fa fa-user"></i><a href="#" class="color-2">by: tiex</a></span>
                        <time datetime="2017-04-17"><i class="fa fa-calendar"></i>17 April, 2017</time>
                        <span><i class="fa fa-comment"></i>5 comments</span>
                    </div>
                    <p>In this article, I’d like to reacquaint you do with the humbler work of communication that is the
                        paragraph.</p>
                    <a href="http://sob7y.ml/single-blog" class="read">read more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/blog/3.jpg" alt="b3"/>
                <div class="content">
                    <h4><a href="http://sob7y.ml/single-blog">easy with app use want to</a></h4>
                    <div class="info">
                        <span><i class="fa fa-user"></i><a href="#" class="color-2">by: tiex</a></span>
                        <time datetime="2017-04-17"><i class="fa fa-calendar"></i>17 April, 2017</time>
                        <span><i class="fa fa-comment"></i>5 comments</span>
                    </div>
                    <p>In this article, I’d like to reacquaint you do with the humbler work of communication that is the
                        paragraph.</p>
                    <a href="http://sob7y.ml/single-blog" class="read">read more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/blog/1.jpg" alt="b1"/>
                <div class="content">
                    <h4><a href="http://sob7y.ml/single-blog">easy with app use want to</a></h4>
                    <div class="info">
                        <span><i class="fa fa-user"></i><a href="#" class="color-2">by: tiex</a></span>
                        <time datetime="2017-04-17"><i class="fa fa-calendar"></i>17 April, 2017</time>
                        <span><i class="fa fa-comment"></i>5 comments</span>
                    </div>
                    <p>In this article, I’d like to reacquaint you do with the humbler work of communication that is the
                        paragraph.</p>
                    <a href="http://sob7y.ml/single-blog" class="read">read more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/blog/2.jpg" alt="b2"/>
                <div class="content">
                    <h4><a href="http://sob7y.ml/single-blog">easy with app use want to</a></h4>
                    <div class="info">
                        <span><i class="fa fa-user"></i><a href="#" class="color-2">by: tiex</a></span>
                        <time datetime="2017-04-17"><i class="fa fa-calendar"></i>17 April, 2017</time>
                        <span><i class="fa fa-comment"></i>5 comments</span>
                    </div>
                    <p>In this article, I’d like to reacquaint you do with the humbler work of communication that is the
                        paragraph.</p>
                    <a href="http://sob7y.ml/single-blog" class="read">read more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/blog/3.jpg" alt="b3"/>
                <div class="content">
                    <h4><a href="http://sob7y.ml/single-blog">easy with app use want to</a></h4>
                    <div class="info">
                        <span><i class="fa fa-user"></i><a href="#" class="color-2">by: tiex</a></span>
                        <time datetime="2017-04-17"><i class="fa fa-calendar"></i>17 April, 2017</time>
                        <span><i class="fa fa-comment"></i>5 comments</span>
                    </div>
                    <p>In this article, I’d like to reacquaint you do with the humbler work of communication that is the
                        paragraph.</p>
                    <a href="http://sob7y.ml/single-blog" class="read">read more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
            <div class="box">
                <img src="/landing/images/blog/1.jpg" alt="b1"/>
                <div class="content">
                    <h4><a href="http://sob7y.ml/single-blog">easy with app use want to</a></h4>
                    <div class="info">
                        <span><i class="fa fa-user"></i><a href="#" class="color-2">by: tiex</a></span>
                        <time datetime="2017-04-17"><i class="fa fa-calendar"></i>17 April, 2017</time>
                        <span><i class="fa fa-comment"></i>5 comments</span>
                    </div>
                    <p>In this article, I’d like to reacquaint you do with the humbler work of communication that is the
                        paragraph.</p>
                    <a href="http://sob7y.ml/single-blog" class="read">read more <i class="fa fa-angle-right"></i></a>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="view">
                <a href="http://sob7y.ml/blog" class="btn more color-1-bg">view more</a>
            </div>
        </div>
    </div>
</section>
 */ ?>
<!-- End Blog -->
<!-- Start Faq -->
<?php /*
<section class="faq p100" id="faq">
    <div class="container">
        <div class="title text-center">
            <i class="icon-clipboard color-1"></i>
            <h2>faq <span class="bg-text">faq</span></h2>
            <p>Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Sed Feugiat Arcu Ut Orci Porta, Eget Porttitor
                Felis Suscipit. Sed A Nisl.</p>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="box">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <!-- Start Panel 1 -->
                        <div class="panel">
                            <div class="panel-heading gradient" role="tab" id="heading_1">
                                <h4 class="panel-title">
                                    <a class="" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse_1" aria-expanded="true" aria-controls="collapse_1">
                                        Lorem ipsum dolor sit amet ?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse_1" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="heading_1">
                                <div class="panel-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum tenetur dicta
                                    commodi, totam atque fugit ut magnam laboriosam dignissimos dolorum minus quia sed
                                    distinctio in mollitia laborum sint delectus accusamus!
                                </div>
                            </div>
                        </div>
                        <!-- End Panel 1 -->
                        <!-- Start Panel 2 -->
                        <div class="panel">
                            <div class="panel-heading gradient" role="tab" id="heading_2">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse_2" aria-expanded="true" aria-controls="collapse_2">
                                        Lorem ipsum dolor sit amet?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse_2" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading_2">
                                <div class="panel-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum tenetur dicta
                                    commodi, totam atque fugit ut magnam laboriosam dignissimos dolorum minus quia sed
                                    distinctio in mollitia laborum sint delectus accusamus!
                                </div>
                            </div>
                        </div>
                        <!-- End Panel 2 -->
                        <!-- Start Panel 3 -->
                        <div class="panel">
                            <div class="panel-heading gradient" role="tab" id="heading_3">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse_3" aria-expanded="true" aria-controls="collapse_3">
                                        Lorem ipsum dolor sit amet?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse_3" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading_3">
                                <div class="panel-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum tenetur dicta
                                    commodi, totam atque fugit ut magnam laboriosam dignissimos dolorum minus quia sed
                                    distinctio in mollitia laborum sint delectus accusamus!
                                </div>
                            </div>
                        </div>
                        <!-- End Panel 3 -->
                        <!-- Start Panel 4 -->
                        <div class="panel">
                            <div class="panel-heading gradient" role="tab" id="heading_4">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                       href="#collapse_4" aria-expanded="true" aria-controls="collapse_4">
                                        Lorem ipsum dolor sit amet?
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse_4" class="panel-collapse collapse" role="tabpanel"
                                 aria-labelledby="heading_4">
                                <div class="panel-body">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Harum tenetur dicta
                                    commodi, totam atque fugit ut magnam laboriosam dignissimos dolorum minus quia sed
                                    distinctio in mollitia laborum sint delectus accusamus!
                                </div>
                            </div>
                        </div>
                        <!-- End Panel 4 -->
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="image box">
                    <img src="/landing/images/faq.png" alt="Faq Image"/>
                </div>
            </div>
        </div>
    </div>
</section>
 */ ?>
<!-- End Faq -->
<!-- Start Contact -->
<section class="contact p100" id="contact">
    <div class="overlay gradient"></div>
    <div class="container">
        <div class="title text-center">
            <i class="icon-chat color-1"></i>
            <h2>Контакты <span class="bg-text">Присоединиться</span></h2>
            <p>При возникновении вопросов о внедрении системы заполните форму и мы обязательно свяжемся с Вами по
                указанным контактным данным.</p>
        </div>
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="box">
                    <form method="post" onsubmit="yaCounter53043121.reachGoal('formcontact'); return true;">
                        <div class="col-md-6">
                            <input type="text" class="form-control name" placeholder="Имя *" required/>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control email" placeholder="E-mail или номер телефона *"
                                   required/>
                        </div>
                        <div class="col-md-12 text-center">
                            <textarea class="form-control message" placeholder="Сообщение"></textarea>
                            <div class="g-recaptcha"
                                 data-sitekey="6Ld16ZoUAAAAANYgOtGZlMbqcinWpZ9NcG634EOx"></div>
                            <input type="submit" class="btn send color-1-bg" onclick="yaCounter53043121.reachGoal('sendbutton'); return true;" value="Отправить"/>
                            <div class="output"></div>
                        </div>
                    </form>
                </div>
            </div>
            <?php /*
            <div class="col-md-5">
                <div class="box info-box">
                                        <div class="item">
                                            <i class="fa fa-map-marker"></i>
                                            <h4>address</h4>
                                            <span>10, park street, london, UK.</span>
                                        </div>
                                        <div class="item">
                                            <i class="fa fa-phone"></i>
                                            <h4>Телефон</h4>
                                            <span><a href="tel:+79290232777">+7 (929) 023 27 77</a></span>
                                        </div>
                    <div class="item">
                        <i class="fa fa-envelope-o"></i>
                        <h4>E-mail</h4>
                        <span><a href="mailto:gendir@it-kit.org" target="_blank">gendir@it-kit.org</a></span>
                    </div>
                    <div class="item">
                        <i class="fa fa-clock-o"></i>
                        <h4>working hours</h4>
                        <span>mon-fri 8.00AM to 5.00PM.</span>
                    </div>
                    <div class="icons">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-google-plus"></i></a>
                        <a href="#"><i class="fa fa-linkedin"></i></a>
                        <a href="#"><i class="fa fa-behance"></i></a>
                    </div>
                </div>
            </div>
            */ ?>
        </div>
    </div>
</section>
<!-- End Contact -->
<!-- Start Subscribe -->
<?php /*
<section class="subscribe p100 text-center">
    <div class="container">
        <div class="title">
            <i class="icon-envelope color-1"></i>
            <h2>subscribe <span class="bg-text">subscribe</span></h2>
            <p>Lorem Ipsum Dolor Sit Amet, Consectetur Adipiscing Elit. Sed Feugiat Arcu Ut Orci Porta, Eget Porttitor
                Felis Suscipit. Sed A Nisl.</p>
        </div>
        <form id="mach-form">
            <div class="form-group">
                <input type="email" id="email" class="form-control" placeholder="email *"/>
                <input type="submit" class="btn subscribe-btn color-1-bg" value="subscribe"/>
            </div>
            <div id="message-newsletter">
                <label for="email" class="message"></label>
            </div>
        </form>
    </div>
</section>
 */ ?>
<!-- End Subscribe -->
<!-- Start Footer -->
<footer class="text-center">
    <div class="container">
        <p><?= Yii::$app->name ?> © <?= date('Y') ?> Все права защищены</p>
    </div>
</footer>
<!-- End Footer -->
<!-- jQuery -->
<script src="/landing/js/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="/landing/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="/landing/js/waypoints.min.js"></script>
<!-- CounterUp -->
<script src="/landing/js/jquery.counterup.min.js"></script>
<!-- Owl-Carousel -->
<script src="/landing/js/owl.carousel.min.js"></script>
<!-- Wow -->
<script src="/landing/js/wow.min.js"></script>
<!-- Magnific-PopUp -->
<script src="/landing/js/jquery.magnific-popup.min.js"></script>
<!-- Easing -->
<script src="/landing/js/jquery.easing.1.3.min.js"></script>
<!-- Google Recaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Custom Js -->
<script src="/landing/js/custom.js"></script>
</body>
</html>