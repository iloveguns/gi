<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\PlannedWorkTask */

$this->title = 'Задача по ТО №' . $model->plannedWork->id;
$this->params['breadcrumbs'][] = [
    'label' => $model->plannedWork->machine->name,
    'url' => ['/machine/view', 'id' => $model->plannedWork->machine_id]
];
$this->params['breadcrumbs'][] = [
    'label' => $model->plannedWork->getName(),
    'url' => ['/planned-work/view', 'id' => $model->planned_work_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="planned-work-task-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
