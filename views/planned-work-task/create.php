<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\PlannedWorkTask */

$this->title = 'Добавить задачу к ТО';
$this->params['breadcrumbs'][] = [
    'label' => $model->plannedWork->machine->name,
    'url' => ['/machine/view', 'id' => $model->plannedWork->machine_id]
];
$this->params['breadcrumbs'][] = [
    'label' => $model->plannedWork->getName(),
    'url' => ['/planned-work/view', 'id' => $model->planned_work_id]
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="planned-work-task-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
