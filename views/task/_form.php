<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use app\models\data\User;
use app\models\data\Task;

/* @var $this yii\web\View */
/* @var $model app\models\data\Task */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-form">

    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'user_id')->widget(Select2::class, [
                        'data' => ArrayHelper::map(User::getUserListByRole(User::ROLE_ENGINEER), 'id',
                            'profile.nameWithPosition'),
                        'options' => ['placeholder' => 'Выберите исполнителя'],
                    ]) ?>
                </div>
            </div>

            <?= $form->field($model, 'notes')->textarea(['rows' => 6, 'maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
