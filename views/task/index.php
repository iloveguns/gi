<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\select2\Select2;
use app\models\data\User;
use yii\helpers\ArrayHelper;
use app\models\data\Machine;
use app\models\data\Task;
use app\models\data\Inspection;
use app\models\data\Work;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\TaskSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $title string */
/* @var $breadcrumbs array */

$this->title = $title . '*';
$this->params['breadcrumbs'] = $breadcrumbs;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index">
    <?php Pjax::begin(); ?>

    <?php $columns = []; ?>
    <?php if (Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
        <?php $columns[] = [
            'attribute' => 'user_id',
            'value' => 'user.profile.name',
            'filter' => Select2::widget([
                'model' => $searchModel,
                'attribute' => 'user_id',
                'data' => ArrayHelper::map(User::getUserListByRole([User::ROLE_ENGINEER]), 'id',
                    'profile.nameWithPosition'),
                'options' => ['prompt' => ''],
                'pluginOptions' => ['allowClear' => true]
            ])
        ]; ?>
    <?php endif; ?>

    <div class="box box-default">
        <div class="box-body">
            <p class="text-right">*Список задач на 14 дней</p>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER) ? $searchModel : null,
                'summary' => false,
                'rowOptions' => function (Task $model) {
                    return $model->isOverdue() ? ['class' => 'bg-red'] : [];
                },
                'columns' => ArrayHelper::merge([
                    [
                        'attribute' => 'created_at',
                        'format' => 'datetime',

                        'filter' => false
                    ],
                    [
                        'filter' => false,
                        'format' => 'raw',
                        'label' => 'Запланировано к выполнению (дата/изготовлено шт)',
                        'value' => function (Task $model) {
                            if ($model->planned_completed_at) {
                                return Yii::$app->formatter->asDatetime($model->planned_completed_at);
                            } elseif ($model->model == Task::MODEL_WORK) {
                                $workModel = Work::findOne($model->model_id);
                                return $workModel ? $workModel->getPlannedAtStr() : '';
                            }

                            return '';
                        }
                    ],
                    [
                        'attribute' => 'model_id',
                        'label' => 'Задача',
                        'format' => 'raw',
                        'filter' => false,
                        'value' => function (Task $model) {
                            return Html::a($model->getName(), $model->getUrl(), ['target' => '_blank']);
                        }
                    ],
                    [
                        'attribute' => 'machine_id',
                        'format' => 'raw',
                        'value' => function (Task $model) {
                            return Html::a($model->machine->name, $model->machine->getUrl(), ['target' => '_blank']);
                        },
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'machine_id',
                            'data' => ArrayHelper::map(Machine::find()->all(), 'id', 'name'),
                            'options' => ['prompt' => ''],
                            'pluginOptions' => ['allowClear' => true]
                        ])
                    ],
                    [
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => 'statusLabel',
                        'filter' => Select2::widget([
                            'model' => $searchModel,
                            'attribute' => 'status',
                            'data' => Task::getStatusList(),
                            'options' => ['prompt' => '', 'multiple' => true],
                            'pluginOptions' => ['allowClear' => true]
                        ]),
                    ]
                ], $columns),
            ]); ?>
        </div>
    </div>

    <?php Pjax::end(); ?>
</div>
