<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\data\RepairTask;
use kartik\datetime\DateTimePicker;
use yii\helpers\ArrayHelper;
use app\models\data\User;
use app\models\data\Machine;
use kartik\select2\Select2;
use zxbodya\yii2\galleryManager\GalleryManager;

/* @var $this yii\web\View */
/* @var $model app\models\data\RepairTask */
/* @var $form yii\widgets\ActiveForm */

$workUserRole = $model->machine->work_type == Machine::TYPE_ELECTRICAL ? User::ROLE_ELECTRIC : User::ROLE_MECHANIC;
?>

<div class="box box-default">
    <div class="box-body">
        <div class="repair-task-form">

            <?php $form = ActiveForm::begin(); ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'desc')->textarea(['rows' => 6, 'maxlength' => true]) ?>
                    <?= $form->field($model,
                        'work_user_id')
                        ->widget(Select2::class, [
                            'data' => ArrayHelper::map(User::getUserListByRole($workUserRole), 'id', 'profile.nameWithPosition'),
                            'options' => ['placeholder' => 'Выберите исполнителя'],
                        ]) ?>
                    <?= $form->field($model, 'status')->dropDownList(RepairTask::getStatusList()) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'plannedEndedAtStr')->widget(DateTimePicker::class, [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'dd.mm.yyyy hh:ii'
                        ]
                    ]) ?>
                    <?= $form->field($model, 'notes')->textarea(['rows' => 6, 'maxlength' => true]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <?php if ($model->isNewRecord): ?>
                            <p>Вы сможете прикрепить фотографии после сохранения.</p>
                        <?php else: ?>
                            <label>Фотографии</label>
                            <?= GalleryManager::widget([
                                'model' => $model,
                                'behaviorName' => 'galleryBehavior',
                                'apiRoute' => 'work/gallery-api'
                            ]); ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>
    </div>
</div>
