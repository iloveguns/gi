<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\data\RepairTask;
use app\models\data\User;

/* @var $this yii\web\View */
/* @var $model app\models\data\RepairTask */

$this->title = $model->getName();
$this->params['breadcrumbs'][] = [
    'label' => $model->machine->name,
    'url' => ['/machine/view', 'id' => $model->machine_id]
];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$images = $model->getBehavior('galleryBehavior')->getImages();
?>
<div class="repair-task-view">

    <p>
        <?php if ($model->status != RepairTask::STATUS_COMPLETED): ?>
            <?php if (Yii::$app->user->can(User::ROLE_ENGINEER) || Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
                <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?php endif; ?>
        <?php endif; ?>

        <?php if ($model->status == RepairTask::STATUS_CREATED): ?>
            <?php if (Yii::$app->user->id == $model->work_user_id): ?>
                <?= Html::a('Начать работу',
                    ['change-status', 'id' => $model->id, 'status' => RepairTask::STATUS_IN_PROCESS],
                    ['class' => 'btn btn-success']) ?>
            <?php endif; ?>
        <?php elseif ($model->status == RepairTask::STATUS_REJECTED): ?>
            <?php if (Yii::$app->user->id == $model->work_user_id): ?>
                <?= Html::a('Начать работу',
                    ['change-status', 'id' => $model->id, 'status' => RepairTask::STATUS_IN_PROCESS],
                    ['class' => 'btn btn-success']) ?>
            <?php endif; ?>
        <?php elseif ($model->status == RepairTask::STATUS_IN_PROCESS): ?>
            <?php if (Yii::$app->user->id == $model->work_user_id): ?>
                <?= Html::a('Работа выполнена',
                    ['change-status', 'id' => $model->id, 'status' => RepairTask::STATUS_TEST],
                    ['class' => 'btn btn-warning']) ?>
            <?php endif; ?>
        <?php elseif ($model->status == RepairTask::STATUS_TEST): ?>
            <?php if (Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)): ?>
                <?= Html::a('Принять работу',
                    ['change-status', 'id' => $model->id, 'status' => RepairTask::STATUS_COMPLETED],
                    ['class' => 'btn btn-info']) ?>

                <?= Html::a('Отклонить работу',
                    ['change-status', 'id' => $model->id, 'status' => RepairTask::STATUS_REJECTED],
                    ['class' => 'btn btn-danger']) ?>
            <?php endif; ?>
        <?php endif; ?>
    </p>

    <div class="box box-default">
        <div class="box-body">
            <div class="table-responsive">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        [
                            'attribute' => 'created_at',
                            'format' => 'datetime',
                            'filter' => false,
                        ],
                        'machine.name',
                        'desc',
                        [
                            'attribute' => 'work_user_id',
                            'label' => 'Исполнитель',
                            'value' => function (RepairTask $model) {
                                return $model->workUser->profile->name;
                            }
                        ],
                        'planned_ended_at:datetime',
                        'started_at:datetime',
                        'ended_at:datetime',
                        'checked_at:datetime',
                        'notes',
                        [
                            'attribute' => 'statusName',
                            'format' => 'raw',
                            'value' => function (RepairTask $model) {
                                $labelClass = 'success';
                                switch ($model->status) {
                                    case RepairTask::STATUS_CREATED:
                                        $labelClass = 'danger';
                                        break;
                                    case RepairTask::STATUS_IN_PROCESS:
                                        $labelClass = 'primary';
                                        break;
                                    case RepairTask::STATUS_TEST:
                                        $labelClass = 'warning';
                                        break;
                                    case RepairTask::STATUS_COMPLETED:
                                        $labelClass = 'success';
                                        break;
                                    case RepairTask::STATUS_REJECTED:
                                        $labelClass = 'danger';
                                        break;
                                }
                                return Html::tag('span', $model->getStatusName(),
                                    ['class' => 'label label-' . $labelClass]);
                            },
                        ],
                    ],
                ]) ?>

            </div>
        </div>
    </div>

    <?php if (!empty($images)): ?>
        <h3>Фотографии</h3>
        <div class="box box-default">
            <div class="box-body">
                <div class="popup-gallery">
                    <?php foreach ($images as $image): ?>
                        <?= Html::a(Html::img($image->getUrl('preview'),
                            ['width' => 100, 'height' => 100, 'style' => 'object-fit: cover;']),
                            $image->getUrl('medium'), ['title' => $image->description]) ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>
