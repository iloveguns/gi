<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\data\Transaction */

$this->title = 'Транзакция #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Транзакции', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => 'Транзакция #' . $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="transaction-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
