<?php

/* @var $this yii\web\View */
/* @var $model app\models\data\Transaction */

$this->title = 'Создать транзакцию';
$this->params['breadcrumbs'][] = ['label' => 'Транзакции', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="transaction-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
