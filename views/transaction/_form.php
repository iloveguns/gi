<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\data\Transaction;

/* @var $this yii\web\View */
/* @var $model app\models\data\Transaction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="transaction-form">

    <div class="box box-default">
        <div class="box-body">
            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="col-md-4">
                    <?= $form->field($model, 'type')->dropDownList(Transaction::getTypeList()) ?>
                    <?= $form->field($model, 'sum')->textInput() ?>
                </div>
                <div class="col-md-4">
                    <?= $form->field($model, 'notes')->textarea(['rows' => 6, 'maxlength' => true]) ?>
                </div>
            </div>
            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
