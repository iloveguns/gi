<?php

$params = array_merge(
    require(__DIR__.'/params.php'),
    require(__DIR__.'/params-local.php')
);
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'gi-console',
    'name' => 'Главный инженер',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@web' => '',
        '@webroot' => dirname(dirname(__FILE__)) . '/web',
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'formatter' => [
            'class' => 'yii\i18n\Formatter',
            'timeZone' => 'UTC',
            'defaultTimeZone' => 'UTC',
            'nullDisplay' => '',
            'datetimeFormat' => 'dd.MM.YYYY, H:mm',
            'dateFormat' => 'dd.MM.YYYY',
            'currencyCode' => 'RUB'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
    ],
    'modules' => [
        'rbac' => 'dektrium\rbac\RbacConsoleModule',
        'user' => [
            'class' => 'dektrium\user\Module',
            'modelMap' => [
                'User' => 'app\models\data\User',
            ],
            'mailer' => [
                'sender' => 'robot@chief-engineer.ru',
            ],
            'admins' => ['admin'],
        ],
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
