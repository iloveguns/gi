<?php

namespace app\commands;

use Yii;
use app\models\data\Machine;
use app\models\data\Transaction;
use yii\console\Controller;
use yii\console\ExitCode;

class TransactionController extends Controller
{

    public function actionDailyFee()
    {
        $dailyFee = Yii::$app->params['daily_fee'];

        $machineCount = Machine::find()
//            ->andWhere(['status' => Machine::STATUS_ACTIVE])
            ->count();

        if ($machineCount > 0) {
            $transaction = new Transaction([
                'type' => Transaction::TYPE_FEE,
                'sum' => $machineCount * -$dailyFee,
                'notes' => 'Плата за оборудование – ' . $dailyFee . ' руб/дн. Количество оборудования – ' . $machineCount . ' шт.'
            ]);

            if (!$transaction->save()) {
                var_dump($transaction->errors);
            }
        }

        $companyBalance = Transaction::getBalance();
        if (Transaction::getBalance() <= 0) {
            if (!Yii::$app->mailer->compose()
                ->setFrom([Yii::$app->params['mailerSender'] => Yii::$app->name])
                ->setTo(Yii::$app->params['supportEmail'])
                ->setSubject('Баланс приложения исчерпан')
                ->setHtmlBody('Баланс приложения ' . Yii::$app->name . '.' . Yii::$app->params['factory_name'] . ' исчерпан. Текущий баланс  – ' . Yii::$app->formatter->asCurrency($companyBalance) . '.')
                ->send()
            ) {
                var_dump('Cannot send email');
                return ExitCode::UNSPECIFIED_ERROR;
            }
        }

        return ExitCode::OK;
    }
}
