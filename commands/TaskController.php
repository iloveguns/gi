<?php

namespace app\commands;

use Yii;
use app\models\data\Inspection;
use app\models\data\Task;
use app\models\data\Machine;
use yii\console\Controller;
use yii\console\ExitCode;
use app\models\data\User;

class TaskController extends Controller
{

    /**
     * Создать задачу о проведении осмотра
     *
     * @return int
     */
    public function actionCreateMachineInspections()
    {
        $machines = Machine::findAll(['status' => Machine::STATUS_ACTIVE]);

        foreach ($machines as $machine) {
            $inspection = new Inspection([
                'machine_id' => $machine->id,
                'user_id' => $machine->responsible_user_id,
                'status' => Inspection::STATUS_NOT_COMPLETED,
            ]);

            $inspection->save();
        }

        return ExitCode::OK;
    }

    /**
     * Создать задачу о внесении количества произведенных изделий станком
     *
     * @return int
     */
    public function actionCreateMachineProductManufacturedCount()
    {
        $machines = Machine::findAll(['type' => Machine::TYPE_PRODUCTION, 'status' => Machine::STATUS_ACTIVE]);

        foreach ($machines as $machine) {
            $task = new Task([
                'machine_id' => $machine->id,
                'user_id' => $machine->responsible_user_id,
                'model' => Task::MODEL_MACHINE,
                'model_id' => $machine->id,
                'status' => Task::STATUS_NOT_COMPLETED,
                'planned_completed_at' => strtotime('now 18:00:00')
            ]);

            $task->save();
        }

        return ExitCode::OK;
    }
}
