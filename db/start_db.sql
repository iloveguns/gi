-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Мар 20 2019 г., 09:16
-- Версия сервера: 5.7.21-20-log
-- Версия PHP: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `host1683373_gi`
--

-- --------------------------------------------------------

--
-- Структура таблицы `auth_assignment`
--

CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_assignment`
--

INSERT INTO `auth_assignment` (`item_name`, `user_id`, `created_at`) VALUES
('admin', '1', 1551802032),
('chief_engineer', '1', 1552204146),
('engineer', '1', 1552728637),
('mechanic', '1', 1552728637);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item`
--

CREATE TABLE `auth_item` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `type` smallint(6) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `rule_name` varchar(64) COLLATE utf8_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `auth_item`
--

INSERT INTO `auth_item` (`name`, `type`, `description`, `rule_name`, `data`, `created_at`, `updated_at`) VALUES
('admin', 1, '', NULL, NULL, 1550565810, 1550565830),
('chief_engineer', 1, '', NULL, NULL, 1551090857, 1552636816),
('electric', 1, '', NULL, NULL, 1551084585, 1551084585),
('engineer', 1, '', NULL, NULL, 1551092805, 1551092885),
('mechanic', 1, '', NULL, NULL, 1551084570, 1551084570);

-- --------------------------------------------------------

--
-- Структура таблицы `auth_item_child`
--

CREATE TABLE `auth_item_child` (
  `parent` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `child` varchar(64) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `auth_rule`
--

CREATE TABLE `auth_rule` (
  `name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `examination`
--

CREATE TABLE `examination` (
  `id` int(11) NOT NULL,
  `machine_id` int(11) NOT NULL COMMENT 'Оборудование',
  `date` int(11) NOT NULL COMMENT 'Дата освидетельствования',
  `document_number` varchar(255) NOT NULL COMMENT 'Номер документа',
  `status` int(1) DEFAULT '0' COMMENT 'Статус',
  `notify_in_days` int(11) DEFAULT '10' COMMENT 'Предупредить за n дней',
  `notes` varchar(1000) DEFAULT NULL COMMENT 'Заметки',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `gallery_image`
--

CREATE TABLE `gallery_image` (
  `id` int(11) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `ownerId` varchar(255) NOT NULL,
  `rank` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `inspection`
--

CREATE TABLE `inspection` (
  `id` int(11) NOT NULL,
  `machine_id` int(11) NOT NULL COMMENT 'Оборудование',
  `user_id` int(11) NOT NULL COMMENT 'Исполнитель',
  `is_problem_found` int(1) DEFAULT '0' COMMENT 'Найдена проблема',
  `status` int(1) DEFAULT '0' COMMENT 'Статус',
  `notes` varchar(1000) DEFAULT NULL COMMENT 'Заметки',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `machine`
--

CREATE TABLE `machine` (
  `id` int(11) NOT NULL,
  `type` int(1) NOT NULL COMMENT 'Тип оборудования',
  `work_type` int(1) NOT NULL COMMENT 'Тип регламента',
  `responsible_user_id` int(11) NOT NULL COMMENT 'Ответственный за осмотры',
  `status` int(1) DEFAULT '1' COMMENT 'Статус',
  `name` varchar(255) NOT NULL COMMENT 'Название',
  `start_date` int(11) NOT NULL COMMENT 'Дата ввода в эксплуатацию',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `produced_count` int(11) DEFAULT '0' COMMENT 'Количество произведенных изделий',
  `placement` varchar(255) DEFAULT NULL COMMENT 'Место расположения',
  `inventory_number` varchar(255) DEFAULT NULL COMMENT 'Инвентарный номер'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1550563931),
('m010101_100001_init_notifications', 1551119183),
('m140209_132017_init', 1550563937),
('m140403_174025_create_account_table', 1550563937),
('m140504_113157_update_tables', 1550563937),
('m140504_130429_create_token_table', 1550563937),
('m140506_102106_rbac_init', 1550564619),
('m140830_171933_fix_ip_field', 1550563937),
('m140830_172703_change_account_table_name', 1550563937),
('m141222_110026_update_ip_field', 1550563937),
('m141222_135246_alter_username_length', 1550563937),
('m150614_103145_update_social_account_table', 1550563937),
('m150623_212711_fix_username_notnull', 1550563937),
('m151218_234654_add_timezone_to_profile', 1550563937),
('m160929_103127_add_last_login_at_to_user_table', 1550563937),
('m170907_052038_rbac_add_index_on_auth_assignment_user_id', 1550564619),
('m180523_151638_rbac_updates_indexes_without_prefix', 1550564619),
('m190219_160402_create_table_machine', 1550651154),
('m190219_174351_create_table_inspection', 1550651154),
('m190219_181023_create_table_repair_task', 1550651154),
('m190219_210543_create_table_planned_work', 1550651154),
('m190219_212708_create_table_planned_work_task', 1550651154),
('m190220_083002_update_table_repair_task', 1550651484),
('m190220_095836_update_table_machine', 1550656778),
('m190220_100718_create_table_work', 1550657613),
('m190221_051647_create_table_work_task', 1550726415),
('m190225_055759_update_table_machine', 1551074393),
('m190225_062211_update_table_machine', 1551075831),
('m190225_063456_update_table_repair_task', 1551076584),
('m190225_064214_update_table_work', 1551076957),
('m190225_085134_updata_table_repair_task', 1551084873),
('m190225_091005_updata_table_work', 1551085841),
('m190228_183151_update_table_machine', 1551378913),
('m190228_190117_create_table_examination', 1551380885),
('m190303_155123_create_table_transaction', 1551628473),
('m190304_104850_create_table_task', 1551793348),
('m190305_083648_update_table_inspection', 1551793348),
('m190305_091536_update_table_inspection', 1551793348),
('m190305_112000_update_table_task', 1551793348),
('m190305_153758_update_table_task', 1551800371),
('m190308_155237_create_table_gallery_image', 1552060465),
('m190315_073802_update_table_work_and_repair_task', 1552638105),
('m190315_090606_update_table_profile', 1552641890),
('m190318_104746_update_table_work', 1552906703),
('m190318_105214_update_table_work', 1552906703),
('m190318_115546_update_table_task', 1552911509);

-- --------------------------------------------------------

--
-- Структура таблицы `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seen` tinyint(1) NOT NULL DEFAULT '0',
  `read` tinyint(1) NOT NULL DEFAULT '0',
  `user_id` int(11) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` int(11) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `planned_work`
--

CREATE TABLE `planned_work` (
  `id` int(11) NOT NULL,
  `machine_id` int(11) NOT NULL COMMENT 'Оборудование',
  `product_frequency` int(11) DEFAULT NULL COMMENT 'Частота проведения ТО по произведенным изделиям',
  `day_frequency` int(11) DEFAULT NULL COMMENT 'Частота проведения ТО по дням',
  `notes` varchar(1000) DEFAULT NULL COMMENT 'Заметки',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `planned_work_task`
--

CREATE TABLE `planned_work_task` (
  `id` int(11) NOT NULL,
  `planned_work_id` int(11) NOT NULL COMMENT 'Запланированное ТО',
  `name` varchar(255) NOT NULL COMMENT 'Задача',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Должность',
  `public_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gravatar_id` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8_unicode_ci,
  `timezone` varchar(40) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `position`, `public_email`, `gravatar_email`, `gravatar_id`, `location`, `website`, `bio`, `timezone`) VALUES
(1, 'Дмитрий Гуляев', NULL, '', '', 'd41d8cd98f00b204e9800998ecf8427e', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `repair_task`
--

CREATE TABLE `repair_task` (
  `id` int(11) NOT NULL,
  `machine_id` int(11) NOT NULL COMMENT 'Оборудование',
  `work_user_id` int(11) DEFAULT NULL COMMENT 'Исполнитель',
  `status` int(1) DEFAULT '0' COMMENT 'Статус',
  `desc` varchar(1000) DEFAULT NULL COMMENT 'Описание проблемы',
  `planned_ended_at` int(11) DEFAULT NULL COMMENT 'Планируемая дата завершения работ',
  `started_at` int(11) DEFAULT NULL COMMENT 'Дата начала работ',
  `ended_at` int(11) DEFAULT NULL COMMENT 'Дата завершения работ',
  `checked_at` int(11) DEFAULT NULL COMMENT 'Дата принятия работы',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

CREATE TABLE `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `client_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci,
  `code` varchar(32) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `machine_id` int(11) NOT NULL COMMENT 'Оборудование',
  `user_id` int(11) NOT NULL COMMENT 'Исполнитель',
  `model` varchar(255) DEFAULT NULL,
  `model_id` int(11) DEFAULT NULL,
  `planned_completed_at` int(11) DEFAULT NULL COMMENT 'Запланированная дата выполнения',
  `planned_product_count` int(11) DEFAULT NULL COMMENT 'Плановое количество произведенных изделий к ТО',
  `status` int(1) DEFAULT '0' COMMENT 'Статус',
  `notes` varchar(1000) DEFAULT NULL COMMENT 'Заметки',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `type` int(1) DEFAULT '1' COMMENT 'Тип транзакции',
  `sum` int(11) NOT NULL COMMENT 'Сумма транзакции',
  `notes` varchar(1000) DEFAULT NULL COMMENT 'Заметки',
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0',
  `last_login_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`, `last_login_at`) VALUES
(1, 'admin', '777gdv@gmail.com', '$2y$10$GeweW6F3i3FQ.r0yySNl1uvXpnY1jiRd.wY4ce4vgFHODSu2MxKcC', 'N_fYmMCrK5zL2AUnunXMpVli1AoTW5Qj', 123123123, NULL, NULL, '::1', 1550564893, 1553062361, 0, 1553062607);

-- --------------------------------------------------------

--
-- Структура таблицы `work`
--

CREATE TABLE `work` (
  `id` int(11) NOT NULL,
  `type` int(1) NOT NULL DEFAULT '1' COMMENT 'Тип ТО',
  `status` int(1) NOT NULL DEFAULT '1' COMMENT 'Статус',
  `machine_id` int(11) NOT NULL COMMENT 'Оборудование',
  `work_user_id` int(11) NOT NULL COMMENT 'Исполнитель',
  `planned_work_id` int(11) DEFAULT NULL COMMENT 'Плановое ТО',
  `started_at` int(11) DEFAULT NULL COMMENT 'Дата начала ТО',
  `ended_at` int(11) DEFAULT NULL COMMENT 'Дата завершения ТО',
  `planned_ended_at` int(11) DEFAULT NULL COMMENT 'Планируемая дата завершения работ',
  `checked_at` int(11) DEFAULT NULL COMMENT 'Дата принятия работы',
  `planned_product_count` int(11) DEFAULT NULL COMMENT 'Плановое количество произведенных изделий к ТО',
  `notes` varchar(1000) DEFAULT NULL COMMENT 'Замечания',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Структура таблицы `work_task`
--

CREATE TABLE `work_task` (
  `id` int(11) NOT NULL,
  `work_id` int(11) NOT NULL COMMENT 'Проводимое ТО',
  `planned_work_task_id` int(11) NOT NULL COMMENT 'Задача по ТО',
  `status` int(1) DEFAULT '0' COMMENT 'Статус',
  `notes` varchar(1000) DEFAULT NULL COMMENT 'Заметки',
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD PRIMARY KEY (`item_name`,`user_id`),
  ADD KEY `idx-auth_assignment-user_id` (`user_id`);

--
-- Индексы таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD PRIMARY KEY (`name`),
  ADD KEY `rule_name` (`rule_name`),
  ADD KEY `idx-auth_item-type` (`type`);

--
-- Индексы таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD PRIMARY KEY (`parent`,`child`),
  ADD KEY `child` (`child`);

--
-- Индексы таблицы `auth_rule`
--
ALTER TABLE `auth_rule`
  ADD PRIMARY KEY (`name`);

--
-- Индексы таблицы `examination`
--
ALTER TABLE `examination`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-examination-machine_id` (`machine_id`);

--
-- Индексы таблицы `gallery_image`
--
ALTER TABLE `gallery_image`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `inspection`
--
ALTER TABLE `inspection`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-inspection-machine_id` (`machine_id`),
  ADD KEY `fk-inspection-user_id` (`user_id`);

--
-- Индексы таблицы `machine`
--
ALTER TABLE `machine`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-machine-responsible_user_id` (`responsible_user_id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_2` (`user_id`),
  ADD KEY `index_3` (`created_at`),
  ADD KEY `index_4` (`seen`);

--
-- Индексы таблицы `planned_work`
--
ALTER TABLE `planned_work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-planned_work-machine_id` (`machine_id`);

--
-- Индексы таблицы `planned_work_task`
--
ALTER TABLE `planned_work_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-planned_work_task-planned_work` (`planned_work_id`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `repair_task`
--
ALTER TABLE `repair_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-repair_task-machine_id` (`machine_id`),
  ADD KEY `fk-repair_task-work_user_id` (`work_user_id`);

--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD UNIQUE KEY `account_unique_code` (`code`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Индексы таблицы `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-task-machine_id` (`machine_id`),
  ADD KEY `fk-task-user_id` (`user_id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Индексы таблицы `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_username` (`username`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- Индексы таблицы `work`
--
ALTER TABLE `work`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-work-machine_id` (`machine_id`),
  ADD KEY `fk-work-planned_work` (`planned_work_id`),
  ADD KEY `fk-work-work_user_id` (`work_user_id`);

--
-- Индексы таблицы `work_task`
--
ALTER TABLE `work_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk-work_task-work` (`work_id`),
  ADD KEY `fk-work_task-planned_work_task` (`planned_work_task_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `examination`
--
ALTER TABLE `examination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `gallery_image`
--
ALTER TABLE `gallery_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `inspection`
--
ALTER TABLE `inspection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `machine`
--
ALTER TABLE `machine`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `planned_work`
--
ALTER TABLE `planned_work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `planned_work_task`
--
ALTER TABLE `planned_work_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `repair_task`
--
ALTER TABLE `repair_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT для таблицы `work`
--
ALTER TABLE `work`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT для таблицы `work_task`
--
ALTER TABLE `work_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `auth_assignment`
--
ALTER TABLE `auth_assignment`
  ADD CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item`
--
ALTER TABLE `auth_item`
  ADD CONSTRAINT `auth_item_ibfk_1` FOREIGN KEY (`rule_name`) REFERENCES `auth_rule` (`name`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `auth_item_child`
--
ALTER TABLE `auth_item_child`
  ADD CONSTRAINT `auth_item_child_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auth_item_child_ibfk_2` FOREIGN KEY (`child`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `examination`
--
ALTER TABLE `examination`
  ADD CONSTRAINT `fk-examination-machine_id` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `inspection`
--
ALTER TABLE `inspection`
  ADD CONSTRAINT `fk-inspection-machine_id` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-inspection-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `machine`
--
ALTER TABLE `machine`
  ADD CONSTRAINT `fk-machine-responsible_user_id` FOREIGN KEY (`responsible_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `planned_work`
--
ALTER TABLE `planned_work`
  ADD CONSTRAINT `fk-planned_work-machine_id` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `planned_work_task`
--
ALTER TABLE `planned_work_task`
  ADD CONSTRAINT `fk-planned_work_task-planned_work` FOREIGN KEY (`planned_work_id`) REFERENCES `planned_work` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `repair_task`
--
ALTER TABLE `repair_task`
  ADD CONSTRAINT `fk-repair_task-machine_id` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-repair_task-work_user_id` FOREIGN KEY (`work_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `fk-task-machine_id` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-task-user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `work`
--
ALTER TABLE `work`
  ADD CONSTRAINT `fk-work-machine_id` FOREIGN KEY (`machine_id`) REFERENCES `machine` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-work-planned_work` FOREIGN KEY (`planned_work_id`) REFERENCES `planned_work` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-work-work_user_id` FOREIGN KEY (`work_user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `work_task`
--
ALTER TABLE `work_task`
  ADD CONSTRAINT `fk-work_task-planned_work_task` FOREIGN KEY (`planned_work_task_id`) REFERENCES `planned_work_task` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk-work_task-work` FOREIGN KEY (`work_id`) REFERENCES `work` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
