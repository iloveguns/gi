<?php

namespace app\controllers;

use app\models\data\User;
use Yii;
use app\models\data\RepairTask;
use app\models\search\RepairTaskSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RepairTaskController implements the CRUD actions for RepairTask model.
 */
class RepairTaskController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all RepairTask models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RepairTaskSearch();

        if (!Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)) {
            $searchModel->work_user_id = Yii::$app->user->id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RepairTask model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RepairTask model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($machine_id)
    {
        $model = new RepairTask([
            'machine_id' => $machine_id
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RepairTask model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionChangeStatus($id, $status)
    {
        $model = $this->findModel($id);
        $model->status = $status;

        if ($model->status == RepairTask::STATUS_IN_PROCESS) {
            $model->started_at = time();
        } elseif ($model->status == RepairTask::STATUS_TEST) {
            $model->ended_at = time();
        } elseif ($model->status == RepairTask::STATUS_COMPLETED) {
            $model->checked_at = time();
        } elseif ($model->status == RepairTask::STATUS_REJECTED) {
            $model->checked_at = time();
        }

        if (!$model->save()) {
            var_dump($model->errors);
            die;
        }

        if ($model->status == RepairTask::STATUS_REJECTED) {
            Yii::$app->session->addFlash('warning', 'Укажите в заметках причину отклонения работы.');
            return $this->redirect(['repair-task/update', 'id' => $model->id]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing RepairTask model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RepairTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RepairTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RepairTask::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
