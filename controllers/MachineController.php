<?php

namespace app\controllers;

use app\models\data\Inspection;
use app\models\data\PlannedWork;
use app\models\data\RepairTask;
use app\models\data\Work;
use app\models\search\ExaminationSearch;
use app\models\search\InspectionSearch;
use app\models\search\PlannedWorkSearch;
use app\models\search\RepairTaskSearch;
use app\models\search\WorkSearch;
use Yii;
use app\models\data\Machine;
use app\models\search\MachineSearch;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\data\User;
use app\models\notification\WorkNotification;
use yii\filters\AccessControl;

/**
 * MachineController implements the CRUD actions for Machine model.
 */
class MachineController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Machine models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MachineSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Machine model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

//        $user = User::findOne(Yii::$app->user->id);
//        WorkNotification::create(WorkNotification::DAILY_FOR_ENGINEERS, ['user' => $user])->send();

        $examinationSearchModel = new ExaminationSearch(['machine_id' => $model->id]);
        $examinationDataProvider = $examinationSearchModel->search(Yii::$app->request->queryParams);

        $plannedWorkSearchModel = new PlannedWorkSearch(['machine_id' => $model->id]);
        $plannedWorkDataProvider = $plannedWorkSearchModel->search(Yii::$app->request->queryParams);

        $workSearchModel = new WorkSearch(['machine_id' => $model->id]);
        $workDataProvider = $workSearchModel->search(Yii::$app->request->queryParams);

        $inspectionsSearchModel = new InspectionSearch(['machine_id' => $model->id]);
        $inspectionsDataProvider = $inspectionsSearchModel->search(Yii::$app->request->queryParams);

        $repairTasksSearchModel = new RepairTaskSearch(['machine_id' => $model->id]);
        $repairTasksDataProvider = $repairTasksSearchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $model,

            'examinationSearchModel' => $examinationSearchModel,
            'plannedWorkSearchModel' => $plannedWorkSearchModel,
            'workSearchModel' => $workSearchModel,
            'inspectionsSearchModel' => $inspectionsSearchModel,
            'repairTasksSearchModel' => $repairTasksSearchModel,

            'examinationDataProvider' => $examinationDataProvider,
            'inspectionsDataProvider' => $inspectionsDataProvider,
            'workDataProvider' => $workDataProvider,
            'repairTasksDataProvider' => $repairTasksDataProvider,
            'plannedWorkDataProvider' => $plannedWorkDataProvider
        ]);
    }

    public function actionTasks($id)
    {
        $machineModel = $this->findModel($id);
        $tasks = $machineModel->getTasks(Yii::$app->user->id);


        return $this->render('tasks', [
            'machineModel' => $machineModel,
            'tasks' => $tasks
        ]);
    }

    public function actionQrCode($id)
    {
        $model = $this->findModel($id);

        return $this->render('qr-code', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new Machine model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Machine();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Machine model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Machine model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Machine model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Machine the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Machine::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
