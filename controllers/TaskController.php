<?php

namespace app\controllers;

use Yii;
use app\models\data\Task;
use app\models\search\TaskSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\data\User;
use app\models\notification\WorkNotification;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $user_id = Yii::$app->request->get('user_id');

        if ($user_id && Yii::$app->user->isGuest) {
            $user = User::findOne($user_id);
            if ($user) {
                Yii::$app->user->login($user);
            }
        }

        return parent::beforeAction($action);
    }

    public function actionSendDailyNotifications()
    {
        // Отправить уведомления инженерам-рабочим
        $user_ids = Yii::$app->authManager->getUserIdsByRole(User::ROLE_ENGINEER);

        foreach ($user_ids as $user_id) {
            $user = User::findOne($user_id);
            WorkNotification::create(WorkNotification::DAILY_FOR_ENGINEERS, ['user' => $user])->send();
        }

        // Отправить уведомление главному инженеру
        $user_ids = Yii::$app->authManager->getUserIdsByRole(User::ROLE_CHIEF_ENGINEER);

        foreach ($user_ids as $user_id) {
            $user = User::findOne($user_id);
            WorkNotification::create(WorkNotification::DAILY_FOR_CHIEF_ENGINEER, ['user' => $user])->send();
        }
    }

    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TaskSearch(['recent' => true]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        $title = 'Текущие задачи';
        $breadcrumbs = [];

        if ($searchModel->machine_id) {
            $breadcrumbs[] = [
                'label' => $searchModel->machine->name,
                'url' => $searchModel->machine->getUrl()
            ];
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'title' => $title,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    /**
     * Displays a single Task model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Task model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Task model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Task model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
