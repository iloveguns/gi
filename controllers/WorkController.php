<?php

namespace app\controllers;

use app\models\data\Inspection;
use app\models\data\Machine;
use app\models\data\RepairTask;
use app\models\data\User;
use app\models\data\WorkTask;
use app\models\search\WorkTaskSearch;
use Yii;
use app\models\data\Work;
use app\models\search\WorkSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use zxbodya\yii2\galleryManager\GalleryManagerAction;

/**
 * WorkController implements the CRUD actions for Work model.
 */
class WorkController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'gallery-api' => [
                'class' => GalleryManagerAction::class,
                'types' => [
                    'work' => Work::class,
                    'repair-task' => RepairTask::class,
                    'inspection' => Inspection::class,
                    'machine' => Machine::class,
                ]
            ],
        ];
    }

    /**
     * Lists all Work models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkSearch();

        if (!Yii::$app->user->can(User::ROLE_CHIEF_ENGINEER)) {
            $searchModel->work_user_id = Yii::$app->user->id;
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Work model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $workTaskSearchModel = new WorkTaskSearch(['work_id' => $model->id]);
        $workTaskDataProvider = $workTaskSearchModel->search(Yii::$app->request->queryParams);

        return $this->render('view', [
            'model' => $model,
            'workTaskSearchModel' => $workTaskSearchModel,
            'workTaskDataProvider' => $workTaskDataProvider
        ]);
    }

    /**
     * Creates a new Work model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($machine_id)
    {
        $model = new Work([
            'machine_id' => $machine_id
        ]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            // Если ТО запланированное, то прикрепляем к проводимому ТО задачи
            if ($model->type == Work::TYPE_PLANNED && $model->planned_work_id) {

                foreach ($model->plannedWork->plannedWorkTasks as $plannedWorkTask) {
                    $workTask = new WorkTask([
                        'work_id' => $model->id,
                        'planned_work_task_id' => $plannedWorkTask->id,
                        'status' => WorkTask::STATUS_NOT_COMPLETED
                    ]);

                    if (!$workTask->save()) {
                        var_dump($workTask->errors);
                    }
                }

            }

            return $this->redirect(['update', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Work model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionChangeStatus($id, $status)
    {
        $model = $this->findModel($id);
        $model->status = $status;

        if ($model->status == Work::STATUS_IN_PROCESS) {
            $model->started_at = time();
        } elseif ($model->status == Work::STATUS_TEST) {
            $model->ended_at = time();
        } elseif ($model->status == Work::STATUS_COMPLETED) {
            $model->checked_at = time();
        } elseif ($model->status == Work::STATUS_REJECTED) {
            $model->checked_at = time();
        }

        if (!$model->save()) {
            var_dump($model->errors);
            die;
        }

        if ($model->status == Work::STATUS_REJECTED) {
            Yii::$app->session->addFlash('warning', 'Укажите в заметках причину отклонения работы.');
            return $this->redirect(['work/update', 'id' => $model->id]);
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Deletes an existing Work model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Work model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Work the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Work::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
