<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class SiteController extends Controller
{
    public $layout = 'no-auth';
    public $enableCsrfValidation = false;

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        if (Yii::$app->user->isGuest) {
            if (!Yii::$app->params['demo']) {
                return $this->redirect('/user/login');
            }

            $this->layout = 'empty';
            return $this->render('index');
        }

        return $this->redirect('/machine');
    }

    public function actionContact()
    {
        // Google Recaptcha Secret: 6Ld16ZoUAAAAAD7cokfy8gVcWf4pTZtF9xI8kZKG

        $post = Yii::$app->request->post();

        if (!empty($post['email'])) {
            $theme = 'Новое сообщение с демо-версии';
            $message = 'Контакт: ' . $post['email'] . '<br>';

            if (!empty($post['name'])) {
                $message .= 'Имя: ' . $post['name'] . '<br>';
            }

            if (!empty($post['message'])) {
                $message .= 'Сообщение: ' . $post['message'] . '<br>';
            }

            Yii::$app->mailer->compose()
                ->setFrom(Yii::$app->params['sender_email'])
                ->setTo(Yii::$app->params['contact_email'])
                ->setSubject($theme)
                ->setHtmlBody($message)
                ->send();

            return 1;
        }

        return 0;
    }
}
