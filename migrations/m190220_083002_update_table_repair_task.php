<?php

use yii\db\Migration;

/**
 * Class m190220_083002_update_table_repair_task
 */
class m190220_083002_update_table_repair_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('repair_task', 'started_at', $this->integer()->after('desc')->comment('Фактическая дата начала работ'));
        $this->addColumn('repair_task', 'ended_at', $this->integer()->after('started_at')->comment('Фактическая дата завершения работ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190220_083002_update_table_repair_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190220_083002_update_table_repair_task cannot be reverted.\n";

        return false;
    }
    */
}
