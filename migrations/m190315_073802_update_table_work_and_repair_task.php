<?php

use yii\db\Migration;

/**
 * Class m190315_073802_update_table_work_and_repair_task
 */
class m190315_073802_update_table_work_and_repair_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('work', 'checked_at', $this->integer()->after('ended_at')->comment('Дата принятия работы'));
        $this->addColumn('repair_task', 'checked_at', $this->integer()->after('ended_at')->comment('Дата принятия работы'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190315_073802_update_table_work_and_repair_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190315_073802_update_table_work_and_repair_task cannot be reverted.\n";

        return false;
    }
    */
}
