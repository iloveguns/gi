<?php

use yii\db\Migration;

/**
 * Class m190305_153758_update_table_task
 */
class m190305_153758_update_table_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'planned_completed_at',
            $this->integer()->after('model_id')->comment('Запланированная дата выполнения'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190305_153758_update_table_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_153758_update_table_task cannot be reverted.\n";

        return false;
    }
    */
}
