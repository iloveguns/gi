<?php

use yii\db\Migration;

/**
 * Class m190315_090606_update_table_profile
 */
class m190315_090606_update_table_profile extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('profile', 'position', $this->string()->after('name')->comment('Должность'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190315_090606_update_table_profile cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190315_090606_update_table_profile cannot be reverted.\n";

        return false;
    }
    */
}
