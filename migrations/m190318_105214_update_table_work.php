<?php

use yii\db\Migration;

/**
 * Class m190318_105214_update_table_work
 */
class m190318_105214_update_table_work extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('work', 'planned_ended_at',
            $this->integer()->after('ended_at')->comment('Планируемая дата завершения работ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190318_105214_update_table_work cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190318_105214_update_table_work cannot be reverted.\n";

        return false;
    }
    */
}
