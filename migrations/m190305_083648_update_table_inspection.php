<?php

use yii\db\Migration;

/**
 * Class m190305_083648_update_table_inspection
 */
class m190305_083648_update_table_inspection extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('inspection', 'status',
            $this->integer(1)->defaultValue(0)->after('is_problem_found')->comment('Статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190305_083648_update_table_inspection cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_083648_update_table_inspection cannot be reverted.\n";

        return false;
    }
    */
}
