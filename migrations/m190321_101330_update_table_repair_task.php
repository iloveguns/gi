<?php

use yii\db\Migration;

/**
 * Class m190321_101330_update_table_repair_task
 */
class m190321_101330_update_table_repair_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->addColumn('repair_task', 'notes', $this->string(1000)->after('desc')->comment('Заметки'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190321_101330_update_table_repair_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190321_101330_update_table_repair_task cannot be reverted.\n";

        return false;
    }
    */
}
