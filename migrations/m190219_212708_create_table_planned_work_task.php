<?php

use yii\db\Migration;

/**
 * Class m190219_212708_create_table_planned_work_task
 */
class m190219_212708_create_table_planned_work_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('planned_work_task', [
            'id' => $this->primaryKey(),
            'planned_work_id' => $this->integer()->notNull()->comment('Запланированное ТО'),
            'name' => $this->string()->notNull()->comment('Задача'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-planned_work_task-planned_work', 'planned_work_task', 'planned_work_id',
            'planned_work', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190219_212708_create_table_planned_work_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190219_212708_create_table_planned_work_task cannot be reverted.\n";

        return false;
    }
    */
}
