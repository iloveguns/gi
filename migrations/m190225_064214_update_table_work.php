<?php

use yii\db\Migration;

/**
 * Class m190225_064214_update_table_work
 */
class m190225_064214_update_table_work extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('work', 'planned_ended_at', $this->integer()->notNull()->after('ended_at')->comment('Планируемая дата завершения работ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190225_064214_update_table_work cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190225_064214_update_table_work cannot be reverted.\n";

        return false;
    }
    */
}
