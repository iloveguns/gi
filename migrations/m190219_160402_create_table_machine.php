<?php

use yii\db\Migration;

/**
 * Class m190219_160402_create_table_machine
 */
class m190219_160402_create_table_machine extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('machine', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(1)->notNull()->comment('Тип оборудования'),
            'status' => $this->integer(1)->defaultValue(1)->comment('Статус оборудования'),
            'name' => $this->string()->notNull()->comment('Наименование оборудования'),
            'start_date' => $this->integer()->notNull()->comment('Дата ввода в эксплуатацию'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190219_160402_create_table_machine cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190219_160402_create_table_machine cannot be reverted.\n";

        return false;
    }
    */
}
