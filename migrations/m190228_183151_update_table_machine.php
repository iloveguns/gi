<?php

use yii\db\Migration;

/**
 * Class m190228_183151_update_table_machine
 */
class m190228_183151_update_table_machine extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('machine', 'responsible_user_id',
            $this->integer()->notNull()->after('work_type')->comment('Ответственный за осмотры'));

        $this->addForeignKey('fk-machine-responsible_user_id', 'machine', 'responsible_user_id',
            'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190228_183151_update_table_machine cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190228_183151_update_table_machine cannot be reverted.\n";

        return false;
    }
    */
}
