<?php

use yii\db\Migration;

/**
 * Class m190220_095836_update_table_machine
 */
class m190220_095836_update_table_machine extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('machine', 'produced_count',
            $this->integer()->defaultValue(0)->comment('Количество произведенных изделий'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190220_095836_update_table_machine cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190220_095836_update_table_machine cannot be reverted.\n";

        return false;
    }
    */
}
