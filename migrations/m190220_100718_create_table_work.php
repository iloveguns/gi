<?php

use yii\db\Migration;

/**
 * Class m190220_100718_create_table_work
 */
class m190220_100718_create_table_work extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('work', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(1)->defaultValue(1)->notNull()->comment('Тип ТО'),
            'status' => $this->integer(1)->defaultValue(1)->notNull()->comment('Статус'),
            'machine_id' => $this->integer()->notNull()->comment('Оборудование'),
            'planned_work_id' => $this->integer()->comment('Плановое ТО'),
            'started_at' => $this->integer()->comment('Фактическая дата начала ТО'),
            'ended_at' => $this->integer()->comment('Фактическая дата завершения ТО'),
            'notes' => $this->string(1000)->comment('Замечания'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-work-machine_id', 'work', 'machine_id',
            'machine', 'id', 'CASCADE');
        $this->addForeignKey('fk-work-planned_work', 'work', 'planned_work_id',
            'planned_work', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190220_100718_create_table_work cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190220_100718_create_table_work cannot be reverted.\n";

        return false;
    }
    */
}
