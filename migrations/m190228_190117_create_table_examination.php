<?php

use yii\db\Migration;

/**
 * Class m190228_190117_create_table_examination
 */
class m190228_190117_create_table_examination extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('examination', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer()->notNull()->comment('Оборудование'),
            'date' => $this->integer()->notNull()->comment('Дата освидетельствования'),
            'document_number' => $this->string()->notNull()->comment('Номер документа'),
            'status' => $this->integer(1)->defaultValue(0)->comment('Статус'),
            'notify_in_days' => $this->integer()->defaultValue(10)->comment('Предупредить за n дней'),
            'notes' => $this->string(1000)->comment('Заметки'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-examination-machine_id', 'examination', 'machine_id',
            'machine', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190228_190117_create_table_examination cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190228_190117_create_table_examination cannot be reverted.\n";

        return false;
    }
    */
}
