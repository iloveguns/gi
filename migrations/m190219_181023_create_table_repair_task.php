<?php

use yii\db\Migration;

/**
 * Class m190219_181023_create_table_repair_task
 */
class m190219_181023_create_table_repair_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('repair_task', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer()->notNull()->comment('Оборудование'),
            'status' => $this->integer(1)->defaultValue(0)->comment('Статус'),
            'desc' => $this->string(1000)->comment('Описание проблемы'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-repair_task-machine_id', 'repair_task', 'machine_id',
            'machine', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190219_181023_create_table_repair_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190219_181023_create_table_repair_task cannot be reverted.\n";

        return false;
    }
    */
}
