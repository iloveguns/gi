<?php

use yii\db\Migration;

/**
 * Class m190305_091536_update_table_inspection
 */
class m190305_091536_update_table_inspection extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('inspection', 'user_id',
            $this->integer()->notNull()->after('machine_id')->comment('Исполнитель'));

        $this->addForeignKey('fk-inspection-user_id', 'inspection', 'user_id',
            'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190305_091536_update_table_inspection cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_091536_update_table_inspection cannot be reverted.\n";

        return false;
    }
    */
}
