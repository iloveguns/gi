<?php

use yii\db\Schema;
use yii\db\Migration;

class m190308_155237_create_table_gallery_image extends Migration
{
    public $tableName = 'gallery_image';

    public function safeUp()
    {
        $this->createTable('{{%' . $this->tableName . '}}', [
            'id' => $this->primaryKey(),
            'type' => Schema::TYPE_STRING,
            'ownerId' => Schema::TYPE_STRING . ' NOT NULL',
            'rank' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'name' => Schema::TYPE_STRING,
            'description' => Schema::TYPE_TEXT
        ]);
    }

    public function safeDown()
    {
        return false;
    }
}
