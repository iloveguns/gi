<?php

use yii\db\Migration;

/**
 * Class m190320_094638_update_table_examination
 */
class m190320_094638_update_table_examination extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('examination', 'next_date',
            $this->integer()->after('notify_in_days')->comment('Дата следуюещего освидетельствования'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190320_094638_update_table_examination cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190320_094638_update_table_examination cannot be reverted.\n";

        return false;
    }
    */
}
