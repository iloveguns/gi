<?php

use yii\db\Migration;

/**
 * Class m190219_210543_create_table_planned_work
 */
class m190219_210543_create_table_planned_work extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('planned_work', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer()->notNull()->comment('Оборудование'),
            'product_frequency' => $this->integer()->comment('Частота проведения ТО по произведенным изделиям'),
            'day_frequency' => $this->integer()->comment('Частота проведения ТО по дням'),
            'notes' => $this->string(1000)->comment('Заметки'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-planned_work-machine_id', 'planned_work', 'machine_id',
            'machine', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190219_210543_create_table_planned_work cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190219_210543_create_table_planned_work cannot be reverted.\n";

        return false;
    }
    */
}
