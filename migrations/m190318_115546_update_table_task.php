<?php

use yii\db\Migration;

/**
 * Class m190318_115546_update_table_task
 */
class m190318_115546_update_table_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'planned_product_count',
            $this->integer()->after('planned_completed_at')->comment('Плановое количество произведенных изделий к ТО'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190318_115546_update_table_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190318_115546_update_table_task cannot be reverted.\n";

        return false;
    }
    */
}
