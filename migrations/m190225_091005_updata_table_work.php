<?php

use yii\db\Migration;

/**
 * Class m190225_091005_updata_table_work
 */
class m190225_091005_updata_table_work extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('work', 'work_user_id', $this->integer()->notNull()->after('machine_id')->comment('Исполнитель'));

        $this->addForeignKey('fk-work-work_user_id', 'work', 'work_user_id',
            'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190225_091005_updata_table_work cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190225_091005_updata_table_work cannot be reverted.\n";

        return false;
    }
    */
}
