<?php

use yii\db\Migration;

/**
 * Class m190304_104850_create_table_task
 */
class m190304_104850_create_table_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer()->notNull()->comment('Оборудование'),
            'user_id' => $this->integer()->notNull()->comment('Исполнитель'),
            'model' => $this->string(),
            'model_id' => $this->integer(),
            'notes' => $this->string(1000)->comment('Заметки'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer()
        ]);

        $this->addForeignKey('fk-task-machine_id', 'task', 'machine_id',
            'machine', 'id', 'CASCADE');

        $this->addForeignKey('fk-task-user_id', 'task', 'user_id',
            'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190304_104850_create_table_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190304_104850_create_table_task cannot be reverted.\n";

        return false;
    }
    */
}
