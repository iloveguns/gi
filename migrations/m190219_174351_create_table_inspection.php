<?php

use yii\db\Migration;

/**
 * Class m190219_174351_create_table_inspection
 */
class m190219_174351_create_table_inspection extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('inspection', [
            'id' => $this->primaryKey(),
            'machine_id' => $this->integer()->notNull()->comment('Оборудование'),
            'is_problem_found' => $this->integer(1)->defaultValue(0)->comment('Найдена проблема'),
            'notes' => $this->string(1000)->comment('Заметки'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-inspection-machine_id', 'inspection', 'machine_id',
            'machine', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190219_174351_create_table_inspection cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190219_174351_create_table_inspection cannot be reverted.\n";

        return false;
    }
    */
}
