<?php

use yii\db\Migration;

/**
 * Class m190225_085134_updata_table_repair_task
 */
class m190225_085134_updata_table_repair_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('repair_task', 'work_user_id', $this->integer()->after('machine_id')->comment('Исполнитель'));

        $this->addForeignKey('fk-repair_task-work_user_id', 'repair_task', 'work_user_id',
            'user', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190225_085134_updata_table_repair_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190225_085134_updata_table_repair_task cannot be reverted.\n";

        return false;
    }
    */
}
