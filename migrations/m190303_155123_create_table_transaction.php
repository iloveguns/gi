<?php

use yii\db\Migration;

/**
 * Class m190303_155123_create_table_transaction
 */
class m190303_155123_create_table_transaction extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('transaction', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(1)->defaultValue(1)->comment('Тип транзакции'),
            'sum' => $this->integer()->notNull()->comment('Сумма транзакции'),
            'notes' => $this->string(1000)->comment('Заметки'),
            'created_at' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190303_155123_create_table_transaction cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190303_155123_create_table_transaction cannot be reverted.\n";

        return false;
    }
    */
}
