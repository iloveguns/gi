<?php

use yii\db\Migration;

/**
 * Class m190305_112000_update_table_task
 */
class m190305_112000_update_table_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('task', 'status', $this->integer(1)->defaultValue(0)->after('model_id')->comment('Статус'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190305_112000_update_table_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190305_112000_update_table_task cannot be reverted.\n";

        return false;
    }
    */
}
