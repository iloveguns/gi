<?php

use yii\db\Migration;

/**
 * Class m190225_063456_update_table_repair_task
 */
class m190225_063456_update_table_repair_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('repair_task', 'planned_ended_at', $this->integer()->after('desc')->comment('Планируемая дата завершения работ'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190225_063456_update_table_repair_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190225_063456_update_table_repair_task cannot be reverted.\n";

        return false;
    }
    */
}
