<?php

use yii\db\Migration;

/**
 * Class m190318_104746_update_table_work
 */
class m190318_104746_update_table_work extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('work', 'planned_product_count', $this->integer()->after('planned_ended_at')->comment('Плановое количество произведенных изделий к ТО'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190318_104746_update_table_work cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190318_104746_update_table_work cannot be reverted.\n";

        return false;
    }
    */
}
