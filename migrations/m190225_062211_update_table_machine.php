<?php

use yii\db\Migration;

/**
 * Class m190225_062211_update_table_machine
 */
class m190225_062211_update_table_machine extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('machine', 'placement', $this->string()->comment('Место расположения'));
        $this->addColumn('machine', 'inventory_number', $this->string()->comment('Инвентарный номер'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190225_062211_update_table_machine cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190225_062211_update_table_machine cannot be reverted.\n";

        return false;
    }
    */
}
