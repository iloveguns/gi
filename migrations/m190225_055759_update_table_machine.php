<?php

use yii\db\Migration;

/**
 * Class m190225_055759_update_table_machine
 */
class m190225_055759_update_table_machine extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('machine', 'work_type',
            $this->integer(1)->notNull()->after('type')->comment('Тип регламента'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190225_055759_update_table_machine cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190225_055759_update_table_machine cannot be reverted.\n";

        return false;
    }
    */
}
