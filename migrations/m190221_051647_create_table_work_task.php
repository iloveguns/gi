<?php

use yii\db\Migration;

/**
 * Class m190221_051647_create_table_work_task
 */
class m190221_051647_create_table_work_task extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('work_task', [
            'id' => $this->primaryKey(),
            'work_id' => $this->integer()->notNull()->comment('Проводимое ТО'),
            'planned_work_task_id' => $this->integer()->notNull()->comment('Задача по ТО'),
            'status' => $this->integer(1)->defaultValue(0)->comment('Статус'),
            'notes' => $this->string(1000)->comment('Заметки'),

            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer()
        ]);

        $this->addForeignKey('fk-work_task-work', 'work_task', 'work_id',
            'work', 'id', 'CASCADE');

        $this->addForeignKey('fk-work_task-planned_work_task', 'work_task', 'planned_work_task_id',
            'planned_work_task', 'id', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190221_051647_create_table_work_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190221_051647_create_table_work_task cannot be reverted.\n";

        return false;
    }
    */
}
