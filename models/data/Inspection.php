<?php

namespace app\models\data;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;
use yii\helpers\Html;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "inspection".
 *
 * @property int $id
 * @property int $machine_id Оборудование
 * @property int $user_id Исполнитель
 * @property int $is_problem_found Проблема найдена
 * @property int $status Статус
 * @property string $notes Заметки
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Machine $machine
 * @property User $user
 * @property Task $task
 */
class Inspection extends \yii\db\ActiveRecord
{
    const STATUS_NOT_COMPLETED = 0;
    const STATUS_COMPLETED = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inspection';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class
            ],
            'galleryBehavior' => [
                'class' => GalleryBehavior::class,
                'type' => 'inspection',
                'extension' => 'jpg',
                'directory' => Yii::getAlias('@webroot') . '/uploads/inspection/photos',
                'url' => Yii::getAlias('@web') . '/uploads/inspection/photos',
                'hasName' => false,
                'versions' => [
                    'medium' => function ($img) {
                        /** @var \Imagine\Image\ImageInterface $img */
                        $dstSize = $img->getSize();
                        $maxWidth = 800;
                        if ($dstSize->getWidth() > $maxWidth) {
                            $dstSize = $dstSize->widen($maxWidth);
                        }
                        return $img
                            ->copy()
                            ->resize($dstSize);
                    },
                ]
            ]
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->createTask();
        }

        $this->updateTask();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['machine_id', 'user_id'], 'required'],
            [
                [
                    'machine_id',
                    'user_id',
                    'status',
                    'is_problem_found',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['notes'], 'string', 'max' => 1000],
            ['is_problem_found', 'default', 'value' => 0],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id']
            ],
            [
                ['machine_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Machine::class,
                'targetAttribute' => ['machine_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'machine_id' => 'Оборудование',
            'user_id' => 'Исполнитель',
            'is_problem_found' => 'Найдена проблема',
            'status' => 'Статус',
            'notes' => 'Заметки',
            'created_at' => 'Дата проведения',
            'updated_at' => 'Updated At',
            'created_by' => 'Ответственный',
            'updated_by' => 'Updated By',
        ];
    }

    public function getName()
    {
        return 'Осмотр оборудования от ' . Yii::$app->formatter->asDate($this->created_at);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['model_id' => 'id'])
            ->andWhere(['model' => Task::MODEL_INSPECTION]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine()
    {
        return $this->hasOne(Machine::class, ['id' => 'machine_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public static function getStatusList()
    {
        return [
            static::STATUS_NOT_COMPLETED => 'Не проведен',
            static::STATUS_COMPLETED => 'Проведен'
        ];
    }

    public function getStatusName()
    {
        $arr = static::getStatusList();
        return $arr[$this->status];
    }

    public function getUrl()
    {
        return Url::to(['/inspection/view', 'id' => $this->id], true);
    }

    public function createTask()
    {
        $task = new Task([
            'machine_id' => $this->machine_id,
            'user_id' => $this->machine->responsible_user_id,
            'model' => Task::MODEL_INSPECTION,
            'model_id' => $this->id,
            'status' => Task::STATUS_NOT_COMPLETED,
            'planned_completed_at' => strtotime('now 18:00:00')
        ]);

        $task->save();
    }

    public function updateTask()
    {
        $model = $this->task;

        if ($model) {
            // Обновляем статус
            switch ($this->status) {
                case static::STATUS_NOT_COMPLETED:
                    $model->status = Task::STATUS_NOT_COMPLETED;
                    break;
                case static::STATUS_COMPLETED:
                    $model->status = Task::STATUS_COMPLETED;
                    break;
            }

            $model->save();
        }
    }
}
