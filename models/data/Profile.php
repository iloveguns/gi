<?php
/**
 * Created by PhpStorm.
 * User: nazmutdinov
 * Date: 15/03/2019
 * Time: 16:15
 */

namespace app\models\data;

use yii\helpers\ArrayHelper;

/**
 * Class Profile
 * @package app\models\data
 *
 * @property string $position
 */
class Profile extends \dektrium\user\models\Profile
{

    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            ['position', 'string', 'max' => 255]
        ]);
    }

    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'position' => 'Должность'
        ]);
    }

    public function getNameWithPosition()
    {
        $str = $this->name;

        if (!empty($this->position)) {
            $str .= ' (' . $this->position . ')';
        }

        return $str;
    }
}