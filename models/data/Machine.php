<?php

namespace app\models\data;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\Url;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "machine".
 *
 * @property int $id
 * @property int $type Тип оборудования
 * @property int $work_type Тип регламента
 * @property int $responsible_user_id
 * @property int $status Статус
 * @property int $produced_count Количество произведенных изделий (шт)
 * @property string $name Наименование
 * @property int $start_date Дата ввода в эксплуатацию
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property string $placement
 * @property string $inventory_number
 *
 * @property Inspection[] $inspections
 * @property RepairTask[] $repairTasks
 * @property User $responsibleUser
 */
class Machine extends \yii\db\ActiveRecord
{
    const STATUS_NOT_ACTIVE = 0;
    const STATUS_ACTIVE = 1;

    const WORK_TYPE_DETAILS_COUNT = 1;
    const WORK_TYPE_PERIODS = 2;
    const WORK_TYPE_INSPECTIONS = 3;


    const TYPE_PRODUCTION = 1;
    const TYPE_ELECTRICAL = 2;
    const TYPE_PRESSURE = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'machine';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class
            ],
            'galleryBehavior' => [
                'class' => GalleryBehavior::class,
                'type' => 'machine',
                'extension' => 'jpg',
                'directory' => Yii::getAlias('@webroot') . '/uploads/machine/photos',
                'url' => Yii::getAlias('@web') . '/uploads/machine/photos',
                'hasName' => false,
                'versions' => [
                    'medium' => function ($img) {
                        /** @var \Imagine\Image\ImageInterface $img */
                        $dstSize = $img->getSize();
                        $maxWidth = 800;
                        if ($dstSize->getWidth() > $maxWidth) {
                            $dstSize = $dstSize->widen($maxWidth);
                        }
                        return $img
                            ->copy()
                            ->resize($dstSize);
                    },
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'work_type', 'name', 'responsible_user_id', 'start_date', 'startDateStr'], 'required'],
            [
                [
                    'type',
                    'work_type',
                    'status',
                    'start_date',
                    'produced_count',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                    'responsible_user_id'
                ],
                'integer'
            ],
            [['name', 'placement', 'inventory_number'], 'string', 'max' => 255],
            [['startDateStr'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип оборудования',
            'typeName' => 'Тип оборудования',
            'work_type' => 'Тип регламента',
            'workTypeName' => 'Тип регламента',
            'status' => 'Статус оборудования',
            'statusName' => 'Статус оборудования',
            'name' => 'Наименование оборудования',
            'produced_count' => 'Количество произведенных изделий (шт)',
            'start_date' => 'Дата ввода в эксплуатацию',
            'startDateStr' => 'Дата ввода в эксплуатацию',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'placement' => 'Место расположения',
            'inventory_number' => 'Инвентарный номер',
            'responsible_user_id' => 'Ответственный за осмотры'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsibleUser()
    {
        return $this->hasOne(User::class, ['id' => 'responsible_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInspections()
    {
        return $this->hasMany(Inspection::class, ['machine_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairTasks()
    {
        return $this->hasMany(RepairTask::class, ['machine_id' => 'id']);
    }

    public function getStartDateStr()
    {
        return $this->start_date ? date('d.m.Y', $this->start_date) : null;
    }

    public function setStartDateStr($val)
    {
        $this->start_date = !empty($val) ? strtotime($val) : null;
    }

    public static function getStatusList()
    {
        return [
            static::STATUS_NOT_ACTIVE => 'Не эксплатируется',
            static::STATUS_ACTIVE => 'В эксплуатации'
        ];
    }

    public function getStatusName()
    {
        $arr = static::getStatusList();
        return $arr[$this->status];
    }

    public static function getTypeList()
    {
        return [
            static::TYPE_PRODUCTION => 'Производственное оборудование',
            static::TYPE_ELECTRICAL => 'Электротехническое оборудование',
            static::TYPE_PRESSURE => 'Сосуды под давлением'
        ];
    }

    public function getTypeName()
    {
        $arr = static::getTypeList();
        return $arr[$this->type];
    }

    public static function getWorkTypeList()
    {
        return [
            static::WORK_TYPE_DETAILS_COUNT => 'Количество производимых изделий',
            static::WORK_TYPE_PERIODS => 'Временные периоды',
            static::WORK_TYPE_INSPECTIONS => 'Только осмотры'
        ];
    }

    public function getWorkTypeName()
    {
        $arr = static::getWorkTypeList();
        return $arr[$this->work_type];
    }

    public function getUrl()
    {
        return Url::to(['/machine/view', 'id' => $this->id], true);
    }

    public function getUpdateUrl()
    {
        return Url::to(['/machine/update', 'id' => $this->id], true);
    }

    public function getTasks($user_id)
    {
        $tasks = [];

        // Осмотр на текущий день
        if ($this->responsible_user_id == $user_id) {
            $todayInspectionExist = Inspection::find()
                ->andWhere(['machine_id' => $this->id])
                ->andWhere(['>=', 'created_at', 'unix_timestamp(curdate())'])
                ->exists();

            if (!$todayInspectionExist) {
                $tasks[] = [
                    'icon' => Html::tag('i', '', ['class' => 'fa fa-eye']),
                    'title' => 'Осмотр оборудования за ' . Yii::$app->formatter->asDate(time()),
                    'url' => Url::to(['/inspection/create', 'machine_id' => $this->id]),
                    'label' => Html::tag('span', 'К выполнению', ['class' => 'label label-warning'])
                ];
            }
        }

        // Проведение ТО
        $works = Work::findAll([
            'machine_id' => $this->id,
            'work_user_id' => $user_id,
            'status' => [Work::STATUS_CREATED, Work::STATUS_IN_PROCESS],
        ]);

        foreach ($works as $work) {
            $labelClass = 'success';
            switch ($work->status) {
                case Work::STATUS_CREATED:
                    $labelClass = 'danger';
                    break;
                case Work::STATUS_IN_PROCESS:
                    $labelClass = 'primary';
                    break;
            }

            $tasks[] = [
                'icon' => Html::tag('i', '', ['class' => 'fas fa-tasks']),
                'title' => $work->getTypeName(),
                'url' => $work->getUrl(),
                'label' => Html::tag('span', $work->getStatusName(), ['class' => 'label label-' . $labelClass])
            ];
        }

        // Задания
        $repairTasks = RepairTask::findAll([
            'machine_id' => $this->id,
            'work_user_id' => $user_id,
            'status' => [RepairTask::STATUS_CREATED, RepairTask::STATUS_IN_PROCESS],
        ]);

        foreach ($repairTasks as $repairTask) {
            $labelClass = 'success';
            switch ($repairTask->status) {
                case RepairTask::STATUS_CREATED:
                    $labelClass = 'danger';
                    break;
                case RepairTask::STATUS_IN_PROCESS:
                    $labelClass = 'primary';
                    break;
            }

            $tasks[] = [
                'icon' => Html::tag('i', '', ['class' => 'fas fa-wrench']),
                'title' => $repairTask->getName(),
                'url' => $repairTask->getUrl(),
                'label' => Html::tag('span', $repairTask->getStatusName(), ['class' => 'label label-' . $labelClass])
            ];
        }

        return $tasks;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * @return Work|null
     */
    public function getLastPlannedWork()
    {
        /**
         * @var $lastWorkModel Work
         */
        $lastWorkModel = Work::find()
            ->andWhere(['machine_id' => $this->id])
            ->andWhere(['type' => Work::TYPE_PLANNED])
            ->addOrderBy(['created_at' => SORT_DESC])
            ->one();

        return $lastWorkModel;
    }
}
