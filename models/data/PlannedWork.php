<?php

namespace app\models\data;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "planned_work".
 *
 * @property int $id
 * @property int $machine_id Оборудование
 * @property int $product_frequency Частота проведения ТО по произведенным изделиям
 * @property int $day_frequency Частота проведения ТО по дням
 * @property string $notes Заметки
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Machine $machine
 * @property PlannedWorkTask[] $plannedWorkTasks
 */
class PlannedWork extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'planned_work';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['machine_id'], 'required'],
            [
                [
                    'machine_id',
                    'product_frequency',
                    'day_frequency',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['notes'], 'string', 'max' => 1000],
            [
                ['machine_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Machine::class,
                'targetAttribute' => ['machine_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'name' => 'ТО',
            'machine_id' => 'Оборудование',
            'product_frequency' => 'Частота проведения ТО по произведенным изделиям (шт)',
            'day_frequency' => 'Частота проведения ТО по времени (дн)',
            'frequencyStr' => 'Частота проведения ТО',
            'notes' => 'Заметки',
            'tasksStr' => 'Задачи',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getName()
    {
        return 'Плановое ТО №' . $this->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine()
    {
        return $this->hasOne(Machine::class, ['id' => 'machine_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlannedWorkTasks()
    {
        return $this->hasMany(PlannedWorkTask::class, ['planned_work_id' => 'id']);
    }

    public function getFrequencyStr()
    {
        if ($this->machine->work_type == Machine::WORK_TYPE_DETAILS_COUNT) {
            return 'каждые ' . $this->product_frequency . ' шт.';
        } elseif ($this->machine->work_type == Machine::WORK_TYPE_PERIODS) {
            return 'каждые ' . $this->day_frequency . ' дн.';
        }
    }

    public function getTasksStr($separator = ', ')
    {
        $str = '';

        foreach ($this->plannedWorkTasks as $plannedWorkTask) {
            $str .= $plannedWorkTask->name . $separator;
        }

        return $str;
    }
}
