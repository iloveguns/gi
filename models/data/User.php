<?php

namespace app\models\data;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class User
 * @package app\models\data
 */
class User extends \dektrium\user\models\User
{
    const ROLE_ADMIN = 'admin';
    const ROLE_CHIEF_ENGINEER = 'chief_engineer';
    const ROLE_ENGINEER = 'engineer';
    const ROLE_MECHANIC = 'mechanic';
    const ROLE_ELECTRIC = 'electric';

    public static function getUserListByRole($role)
    {
        $arr = [];
        $user_ids = [];

        if (is_array($role)) {
            foreach ($role as $roleCode) {
                $user_ids = ArrayHelper::merge($user_ids, Yii::$app->authManager->getUserIdsByRole($roleCode));
            }
        } else {
            $user_ids = Yii::$app->authManager->getUserIdsByRole($role);
        }

        foreach ($user_ids as $user_id) {
            $arr[] = User::findOne($user_id);
        }

        return $arr;
    }
}