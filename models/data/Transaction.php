<?php

namespace app\models\data;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "transaction".
 *
 * @property int $id
 * @property int $type Тип транзакции
 * @property int $sum Сумма транзакции
 * @property string $notes Заметки
 * @property int $created_at
 */
class Transaction extends \yii\db\ActiveRecord
{
    const TYPE_FEE = 1;
    const TYPE_REFILL = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'transaction';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'sum', 'created_at'], 'integer'],
            [['sum'], 'required'],
            [['notes'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип транзакции',
            'typeName' => 'Тип транзакции',
            'sum' => 'Сумма транзакции',
            'notes' => 'Заметки',
            'created_at' => 'Дата',
        ];
    }

    public static function getTypeList()
    {
        return [
            static::TYPE_FEE => 'Абонентская плата',
            static::TYPE_REFILL => 'Пополнение баланса'
        ];
    }

    public function getTypeName()
    {
        $arr = static::getTypeList();
        return $arr[$this->type];
    }

    public static function getBalance()
    {
        $sum = static::find()
            ->sum('sum');

        return $sum ? $sum : 0;
    }
}
