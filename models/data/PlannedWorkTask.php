<?php

namespace app\models\data;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "planned_work_task".
 *
 * @property int $id
 * @property int $planned_work_id Запланированное ТО
 * @property string $name Задача
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property PlannedWork $plannedWork
 */
class PlannedWorkTask extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'planned_work_task';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['planned_work_id', 'name'], 'required'],
            [['planned_work_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['planned_work_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlannedWork::className(), 'targetAttribute' => ['planned_work_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'planned_work_id' => 'Запланированное ТО',
            'name' => 'Описание задачи',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlannedWork()
    {
        return $this->hasOne(PlannedWork::className(), ['id' => 'planned_work_id']);
    }
}
