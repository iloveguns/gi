<?php

namespace app\models\data;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\Url;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "repair_task".
 *
 * @property int $id
 * @property int $machine_id Оборудование
 * @property int $status Статус
 * @property string $desc Описание проблемы
 * @property int started_at Фактическая дата начала работы
 * @property int ended_at Фактическая дата завершения работы
 * @property int $checked_at Дата принятия работы
 * @property int $planned_ended_at Плановая дата завершения ТО
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $work_user_id
 * @property string $notes
 *
 * @property Machine $machine
 * @property User $creator
 * @property User $workUser
 * @property Task $task
 */
class RepairTask extends \yii\db\ActiveRecord
{
    const STATUS_CREATED = 0;
    const STATUS_IN_PROCESS = 1;
    const STATUS_TEST = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_REJECTED = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'repair_task';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class
            ],
            'galleryBehavior' => [
                'class' => GalleryBehavior::class,
                'type' => 'repair-task',
                'extension' => 'jpg',
                'directory' => Yii::getAlias('@webroot') . '/uploads/repair-task/photos',
                'url' => Yii::getAlias('@web') . '/uploads/repair-task/photos',
                'hasName' => false,
                'versions' => [
                    'medium' => function ($img) {
                        /** @var \Imagine\Image\ImageInterface $img */
                        $dstSize = $img->getSize();
                        $maxWidth = 800;
                        if ($dstSize->getWidth() > $maxWidth) {
                            $dstSize = $dstSize->widen($maxWidth);
                        }
                        return $img
                            ->copy()
                            ->resize($dstSize);
                    },
                ]
            ]
        ];
    }

    public function beforeSave($insert)
    {
        // Если сменился статус работы
        if (isset($this->dirtyAttributes['status'])) {
            switch ($this->status) {
                case static::STATUS_IN_PROCESS:
                    $this->started_at = time();
                    break;
                case static::STATUS_TEST:
                    $this->ended_at = time();
                    break;
                case static::STATUS_COMPLETED:
                    $this->checked_at = time();
                    break;
                case static::STATUS_REJECTED:
                    $this->checked_at = time();
                    break;
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->createTask();
        }

        $this->updateTask();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['machine_id', 'work_user_id', 'planned_ended_at', 'plannedEndedAtStr'], 'required'],
            [
                [
                    'machine_id',
                    'started_at',
                    'ended_at',
                    'checked_at',
                    'planned_ended_at',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                    'status',
                    'work_user_id'
                ],
                'integer'
            ],
            [['desc', 'notes'], 'string', 'max' => 1000],
            [
                ['machine_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Machine::class,
                'targetAttribute' => ['machine_id' => 'id']
            ],
            [['startedAtStr', 'endedAtStr', 'plannedEndedAtStr', 'checkedAtStr'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'machine_id' => 'Оборудование',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'desc' => 'Описание проблемы',
            'started_at' => 'Фактическая дата начала работ',
            'startedAtStr' => 'Фактическая дата начала работ',
            'ended_at' => 'Фактическая дата завершения работ',
            'endedAtStr' => 'Фактическая дата завершения работ',
            'checked_at' => 'Дата принятия работы',
            'checkedAtStr' => 'Дата принятия работы',
            'planned_ended_at' => 'Планируемая дата завершения работ',
            'plannedEndedAtStr' => 'Планируемая дата завершения работ',
            'created_at' => 'Дата создания задания',
            'updated_at' => 'Дата обновления',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'work_user_id' => 'Исполнитель',
            'notes' => 'Заметки',
        ];
    }

    public function getStartedAtStr()
    {
        return $this->started_at ? date('d.m.Y H:i', $this->started_at) : null;
    }

    public function setStartedAtStr($val)
    {
        $this->started_at = !empty($val) ? strtotime($val) : null;
    }

    public function getEndedAtStr()
    {
        return $this->ended_at ? date('d.m.Y H:i', $this->ended_at) : null;
    }

    public function setEndedAtStr($val)
    {
        $this->ended_at = !empty($val) ? strtotime($val) : null;
    }

    public function getCheckedAtStr()
    {
        return $this->checked_at ? date('d.m.Y H:i', $this->checked_at) : null;
    }

    public function setCheckedAtStr($val)
    {
        $this->checked_at = !empty($val) ? strtotime($val) : null;
    }

    public function getPlannedEndedAtStr()
    {
        return $this->planned_ended_at ? date('d.m.Y H:i', $this->planned_ended_at) : null;
    }

    public function setPlannedEndedAtStr($val)
    {
        $this->planned_ended_at = !empty($val) ? strtotime($val) : null;
    }

    public static function getStatusList()
    {
        return [
            static::STATUS_CREATED => 'К выполнению',
            static::STATUS_IN_PROCESS => 'В работе',
            static::STATUS_TEST => 'На проверке',
            static::STATUS_COMPLETED => 'Выполнено',
            static::STATUS_REJECTED => 'Отклонено',
        ];
    }

    public function getStatusName()
    {
        $arr = static::getStatusList();
        return $arr[$this->status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine()
    {
        return $this->hasOne(Machine::class, ['id' => 'machine_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkUser()
    {
        return $this->hasOne(User::class, ['id' => 'work_user_id']);
    }

    public function getName()
    {
        return 'Задание №' . $this->id . ' от ' . Yii::$app->formatter->asDate($this->created_at);
    }

    public function getUrl()
    {
        return Url::to(['/repair-task/view', 'id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['model_id' => 'id'])
            ->andWhere(['model' => Task::MODEL_REPAIR_TASK]);
    }

    public function createTask()
    {
        $task = new Task([
            'machine_id' => $this->machine_id,
            'user_id' => $this->work_user_id,
            'model' => Task::MODEL_REPAIR_TASK,
            'model_id' => $this->id,
            'status' => Task::STATUS_NOT_COMPLETED,
            'planned_completed_at' => $this->planned_ended_at
        ]);
        $task->save();
    }

    public function updateTask()
    {
        $model = $this->task;

        if ($model) {
            // Обновляем статус задачи
            switch ($this->status) {
                case static::STATUS_CREATED:
                    $model->status = Task::STATUS_NOT_COMPLETED;
                    break;
                case static::STATUS_IN_PROCESS:
                    $model->status = Task::STATUS_PROCESS;
                    break;
                case static::STATUS_TEST:
                    $model->status = Task::STATUS_TEST;
                    break;
                case static::STATUS_COMPLETED:
                    $model->status = Task::STATUS_COMPLETED;
                    break;
                case static::STATUS_REJECTED:
                    $model->status = Task::STATUS_REJECTED;
                    break;
            }

            // Обновляем ответственного
            if ($this->work_user_id != $model->user_id) {
                $model->user_id = $this->work_user_id;
            }

            // Обновляем планируемую дату выполнения
            if ($this->planned_ended_at != $model->planned_completed_at) {
                $model->planned_completed_at = $this->planned_ended_at;
            }

            if (!$model->save()) {
                var_dump($model->errors);
                die;
            }
        }
    }

    public function getStatusLabel()
    {
        $labelClass = 'default';
        switch ($this->status) {
            case static::STATUS_CREATED:
                $labelClass = 'warning';
                break;
            case static::STATUS_IN_PROCESS:
                $labelClass = 'primary';
                break;
            case static::STATUS_TEST:
                $labelClass = 'warning';
                break;
            case static::STATUS_COMPLETED:
                $labelClass = 'success';
                break;
            case static::STATUS_REJECTED:
                $labelClass = 'danger';
                break;
        }

        return Html::tag('span', $this->getStatusName(),
            ['class' => 'label label-' . $labelClass]);
    }
}
