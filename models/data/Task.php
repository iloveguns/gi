<?php

namespace app\models\data;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property int $machine_id
 * @property int $user_id
 * @property int $status
 * @property string $model
 * @property int $model_id
 * @property string $notes
 * @property int $planned_completed_at
 * @property int $created_at
 * @property int $updated_at
 * @property int $planned_product_count
 *
 * @property User $user
 * @property Machine $machine
 */
class Task extends \yii\db\ActiveRecord
{
    const STATUS_NOT_COMPLETED = 0;
    const STATUS_PROCESS = 1;
    const STATUS_TEST = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_REJECTED = 4;

    const MODEL_WORK = 'Work';
    const MODEL_REPAIR_TASK = 'RepairTask';
    const MODEL_INSPECTION = 'Inspection';
    const MODEL_MACHINE = 'Machine';
    const MODEL_EXAMINATION = 'Examination';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'task';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'model_id',
                    'machine_id',
                    'user_id',
                    'planned_completed_at',
                    'created_at',
                    'updated_at',
                    'status',
                    'planned_product_count'
                ],
                'integer'
            ],
            [['model'], 'string', 'max' => 255],
            [['notes'], 'string', 'max' => 1000],
            [
                ['machine_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Machine::class,
                'targetAttribute' => ['machine_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => User::class,
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'machine_id' => 'Оборудование',
            'user_id' => 'Исполнитель',
            'model' => 'Работа',
            'modelName' => 'Работа',
            'model_id' => 'Работа',
            'planned_completed_at' => 'Планируемая дата выполнения',
            'planned_product_count' => 'Плановое количество произведенных изделий к ТО (шт)',
            'notes' => 'Задача',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'status' => 'Статус',
            'statusLabel' => 'Статус'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine()
    {
        return $this->hasOne(Machine::class, ['id' => 'machine_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    public static function getStatusList()
    {
        return [
            static::STATUS_NOT_COMPLETED => 'К выполнению',
            static::STATUS_PROCESS => 'В работе',
            static::STATUS_TEST => 'На проверке',
            static::STATUS_COMPLETED => 'Выполнена',
            static::STATUS_REJECTED => 'Отклонена',
        ];
    }

    public function getStatusName()
    {
        $arr = static::getStatusList();
        return $arr[$this->status] ?: null;
    }

    public static function getModelNameList()
    {
        return [
            static::MODEL_WORK => 'Техническое обслуживание',
            static::MODEL_REPAIR_TASK => 'Задание',
            static::MODEL_INSPECTION => 'Плановый осмотр',
            static::MODEL_MACHINE => 'Оборудование',
            static::MODEL_EXAMINATION => 'Освидетельствование',
        ];
    }

    public function getModelName()
    {
        $arr = static::getModelNameList();
        return $arr[$this->model] ?: null;
    }

    public static function getModelClassList()
    {
        return [
            static::MODEL_WORK => Work::class,
            static::MODEL_REPAIR_TASK => RepairTask::class,
            static::MODEL_INSPECTION => Inspection::class,
            static::MODEL_MACHINE => Machine::class,
            static::MODEL_EXAMINATION => Examination::class,
        ];
    }

    public function getModelClass()
    {
        $arr = static::getModelClassList();
        return $arr[$this->model] ?: null;
    }

    public function getName()
    {
        $name = '';

        switch ($this->model) {
            case static::MODEL_INSPECTION:
                $model = Inspection::findOne($this->model_id);
                $name = $model ? $model->getName() : null;
                break;
            case static::MODEL_WORK:
                $model = Work::findOne($this->model_id);
                $name = $model ? $model->getName() : null;
                break;
            case static::MODEL_REPAIR_TASK:
                $model = RepairTask::findOne($this->model_id);
                $name = $model ? $model->getName() : null;
                break;
            case static::MODEL_EXAMINATION:
                $model = Examination::findOne($this->model_id);
                $name = $model ? $model->getName() : null;
                break;
            case static::MODEL_MACHINE:
                $model = Machine::findOne($this->model_id);
                $name = 'Внести количество произведенных изделий';
                break;
        }

        return $name;
    }

    public function getUrl()
    {
        $url = '';

        switch ($this->model) {
            case static::MODEL_INSPECTION:
                $model = Inspection::findOne($this->model_id);
                $url = $model ? $model->getUrl() : null;
                break;
            case static::MODEL_WORK:
                $model = Work::findOne($this->model_id);
                $url = $model ? $model->getUrl() : null;
                break;
            case static::MODEL_REPAIR_TASK:
                $model = RepairTask::findOne($this->model_id);
                $url = $model ? $model->getUrl() : null;
                break;
            case static::MODEL_EXAMINATION:
                $model = Examination::findOne($this->model_id);
                $url = $model ? $model->getUrl() : null;
                break;
            case static::MODEL_MACHINE:
                $model = Machine::findOne($this->model_id);
                $url = $model ? $model->getUpdateUrl() : null;
                break;
        }

        return $url;
    }

    public function getStatusLabel()
    {
        $labelClass = 'default';
        switch ($this->status) {
            case static::STATUS_NOT_COMPLETED:
                $labelClass = 'danger';
                break;
            case static::STATUS_PROCESS:
                $labelClass = 'primary';
                break;
            case static::STATUS_TEST:
                $labelClass = 'warning';
                break;
            case static::STATUS_COMPLETED:
                $labelClass = 'success';
                break;
            case static::STATUS_REJECTED:
                $labelClass = 'danger';
                break;
        }

        return Html::tag('span', $this->getStatusName(),
            ['class' => 'label label-' . $labelClass]);
    }

    /**
     * Проверить просрочена ли задача
     *
     * @return bool
     */
    public function isOverdue()
    {
        if ($this->status != Task::STATUS_COMPLETED) {
            if (!empty($this->planned_completed_at)) { // Если у задачи назначена дата выполнения
                return $this->planned_completed_at <= time();
            } elseif (!empty($this->planned_product_count)) { // Если задача запланирована по кол-ву произведенных изделий
                return $this->planned_product_count - $this->machine->produced_count <= 0;
            }
        }

        return false;
    }
}
