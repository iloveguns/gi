<?php

namespace app\models\data;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "work_task".
 *
 * @property int $id
 * @property int $work_id Проводимое ТО
 * @property int $planned_work_task_id Задача по ТО
 * @property int $status Статус
 * @property string $notes Заметки
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property PlannedWorkTask $plannedWorkTask
 * @property Work $work
 */
class WorkTask extends \yii\db\ActiveRecord
{
    const STATUS_NOT_COMPLETED = 0;
    const STATUS_COMPLETED = 1;
    
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_task';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['work_id', 'planned_work_task_id'], 'required'],
            [['work_id', 'planned_work_task_id', 'status', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['notes'], 'string', 'max' => 1000],
            [['planned_work_task_id'], 'exist', 'skipOnError' => true, 'targetClass' => PlannedWorkTask::class, 'targetAttribute' => ['planned_work_task_id' => 'id']],
            [['work_id'], 'exist', 'skipOnError' => true, 'targetClass' => Work::class, 'targetAttribute' => ['work_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'work_id' => 'Проводимое ТО',
            'planned_work_task_id' => 'Задача по ТО',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'notes' => 'Заметки',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public static function getStatusList()
    {
        return [
            static::STATUS_NOT_COMPLETED => 'Не выполнено',
            static::STATUS_COMPLETED => 'Выполнено',
        ];
    }

    public function getStatusName()
    {
        $arr = static::getStatusList();
        return $arr[$this->status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlannedWorkTask()
    {
        return $this->hasOne(PlannedWorkTask::class, ['id' => 'planned_work_task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWork()
    {
        return $this->hasOne(Work::class, ['id' => 'work_id']);
    }
}
