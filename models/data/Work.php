<?php

namespace app\models\data;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Html;
use yii\helpers\Url;
use zxbodya\yii2\galleryManager\GalleryBehavior;

/**
 * This is the model class for table "work".
 *
 * @property int $id
 * @property int $type Тип ТО
 * @property int $status Статус
 * @property int $machine_id Оборудование
 * @property int $planned_work_id Плановое ТО
 * @property int $started_at Фактическая дата начала ТО
 * @property int $ended_at Фактическая дата завершения ТО
 * @property int $checked_at Дата принятия работы
 * @property int $planned_ended_at Плановая дата завершения ТО
 * @property string $notes Замечания
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 * @property int $work_user_id
 * @property int $planned_product_count Плановое количество произведенных изделий к ТО
 *
 * @property Machine $machine
 * @property PlannedWork $plannedWork
 * @property User $creator
 * @property WorkTask[] $workTasks
 * @property User $workUser
 * @property Task $task
 */
class Work extends \yii\db\ActiveRecord
{
    const TYPE_NOT_PLANNED = 1;
    const TYPE_PLANNED = 2;

    const STATUS_CREATED = 0;
    const STATUS_IN_PROCESS = 1;
    const STATUS_TEST = 2;
    const STATUS_COMPLETED = 3;
    const STATUS_REJECTED = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class
            ],
            'galleryBehavior' => [
                'class' => GalleryBehavior::class,
                'type' => 'work',
                'extension' => 'jpg',
                'directory' => Yii::getAlias('@webroot') . '/uploads/work/photos',
                'url' => Yii::getAlias('@web') . '/uploads/work/photos',
                'hasName' => false,
                'versions' => [
                    'medium' => function ($img) {
                        /** @var \Imagine\Image\ImageInterface $img */
                        $dstSize = $img->getSize();
                        $maxWidth = 800;
                        if ($dstSize->getWidth() > $maxWidth) {
                            $dstSize = $dstSize->widen($maxWidth);
                        }
                        return $img
                            ->copy()
                            ->resize($dstSize);
                    },
                ]
            ]
        ];
    }

    public function beforeSave($insert)
    {
        // Если сменился статус работы
        if (isset($this->dirtyAttributes['status'])) {
            switch ($this->status) {
                case static::STATUS_IN_PROCESS:
                    $this->started_at = time();
                    break;
                case static::STATUS_TEST:
                    $this->ended_at = time();
                    break;
                case static::STATUS_COMPLETED:
                    $this->checked_at = time();

                    if ($this->type == static::TYPE_PLANNED && $this->plannedWork) {
                        $nextPlannedWork = new Work([
                            'type' => static::TYPE_PLANNED,
                            'status' => static::STATUS_CREATED,
                            'machine_id' => $this->machine_id,
                            'planned_work_id' => $this->planned_work_id,
                            'work_user_id' => $this->work_user_id,
                        ]);

                        if ($this->machine->work_type == Machine::WORK_TYPE_PERIODS && $this->plannedWork->day_frequency >= 1) {
                            $nextPlannedWork->planned_ended_at = time() + $this->plannedWork->day_frequency * 24 * 3600;
                        } elseif ($this->machine->work_type == Machine::WORK_TYPE_DETAILS_COUNT && $this->plannedWork->product_frequency >= 1) {
                            $nextPlannedWork->planned_product_count = $this->machine->produced_count + $this->plannedWork->product_frequency;
                        }

                        if (!$nextPlannedWork->save()) {
                            var_dump($nextPlannedWork->errors);
                        }
                    }

                    break;
                case static::STATUS_REJECTED:
                    $this->checked_at = time();
                    break;
            }
        }

        // Если не назначен исполнитель, то ставим текущего пользователя
        if (empty($this->work_user_id)) {
            if (!Yii::$app->user->isGuest && Yii::$app->user->can(User::ROLE_ENGINEER)) {
                $this->work_user_id = Yii::$app->user->id;
            } else {
                $this->work_user_id = $this->machine->responsible_user_id;
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $this->createTask();
        }

        $this->updateTask();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'status', 'machine_id', 'work_user_id'], 'required'], // 'planned_ended_at', 'plannedEndedAtStr',
            [
                [
                    'type',
                    'status',
                    'machine_id',
                    'planned_work_id',
                    'started_at',
                    'ended_at',
                    'checked_at',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                    'work_user_id',
                    'planned_product_count'
                ],
                'integer'
            ],
            [['notes'], 'string', 'max' => 1000],
            [
                ['machine_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Machine::class,
                'targetAttribute' => ['machine_id' => 'id']
            ],
            [
                ['planned_work_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => PlannedWork::class,
                'targetAttribute' => ['planned_work_id' => 'id']
            ],
            [['plannedEndedAtStr', 'startedAtStr', 'endedAtStr', 'checkedAtStr'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'type' => 'Тип ТО',
            'typeName' => 'ТО',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'machine_id' => 'Оборудование',
            'machineName' => 'Оборудование',
            'planned_work_id' => 'Плановое ТО',
            'plannedWorkName' => 'Плановое ТО',
            'started_at' => 'Фактическая дата начала ТО',
            'startedAtStr' => 'Фактическая дата начала ТО',
            'ended_at' => 'Фактическая дата завершения ТО',
            'endedAtStr' => 'Фактическая дата завершения ТО',
            'checked_at' => 'Дата принятия работы',
            'checkedAtStr' => 'Дата принятия работы',
            'planned_ended_at' => 'Планируемая дата завершения ТО',
            'plannedEndedAtStr' => 'Планируемая дата завершения ТО',
            'notes' => 'Замечания',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'work_user_id' => 'Исполнитель',
            'progress' => 'Прогресс выполнения',
            'planned_product_count' => 'Плановое количество произведенных изделий к ТО (шт)'
        ];
    }

    public function getProgress()
    {
        $tasksCount = $this->getWorkTasks()->count();
        $completedTasksCount = $this->getWorkTasks()->andWhere(['status' => WorkTask::STATUS_COMPLETED])->count();

        return $tasksCount > 0 ? round($completedTasksCount / $tasksCount * 100, 0) : 0;
    }

    public function getStartedAtStr()
    {
        return $this->started_at ? date('d.m.Y H:i', $this->started_at) : null;
    }

    public function setStartedAtStr($val)
    {
        $this->started_at = !empty($val) ? strtotime($val) : null;
    }

    public function getEndedAtStr()
    {
        return $this->ended_at ? date('d.m.Y H:i', $this->ended_at) : null;
    }

    public function setEndedAtStr($val)
    {
        $this->ended_at = !empty($val) ? strtotime($val) : null;
    }

    public function getCheckedAtStr()
    {
        return $this->checked_at ? date('d.m.Y H:i', $this->checked_at) : null;
    }

    public function setCheckedAtStr($val)
    {
        $this->checked_at = !empty($val) ? strtotime($val) : null;
    }

    public function getPlannedEndedAtStr()
    {
        return $this->planned_ended_at ? date('d.m.Y H:i', $this->planned_ended_at) : null;
    }

    public function setPlannedEndedAtStr($val)
    {
        $this->planned_ended_at = !empty($val) ? strtotime($val) : null;
    }

    public static function getTypeList()
    {
        return [
            static::TYPE_NOT_PLANNED => 'Внеплановое ТО',
            static::TYPE_PLANNED => 'Плановое ТО',
        ];
    }

    public function getTypeName()
    {
        $arr = static::getTypeList();

        if ($this->type == static::TYPE_PLANNED && $this->planned_work_id) {
            $plannedWork = PlannedWork::findOne($this->planned_work_id);
            $name = $plannedWork->getName();
            $name .= $this->started_at ? ' от ' . $this->getStartedAtStr() : '';
            return $name;
        } elseif ($this->type == static::TYPE_NOT_PLANNED) {
            $name = $arr[$this->type] . ' №' . $this->id;
            $name .= $this->started_at ? ' от ' . $this->getStartedAtStr() : '';
            return $name;
        }

        return $arr[$this->type];
    }

    public static function getStatusList()
    {
        return [
            static::STATUS_CREATED => 'К выполнению',
            static::STATUS_IN_PROCESS => 'В работе',
            static::STATUS_TEST => 'На проверке',
            static::STATUS_COMPLETED => 'Выполнено',
            static::STATUS_REJECTED => 'Отклонено',
        ];
    }

    public function getStatusName()
    {
        $arr = static::getStatusList();
        return $arr[$this->status];
    }

    public function getUrl()
    {
        return Url::to(['/work/view', 'id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine()
    {
        return $this->hasOne(Machine::class, ['id' => 'machine_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlannedWork()
    {
        return $this->hasOne(PlannedWork::class, ['id' => 'planned_work_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkTasks()
    {
        return $this->hasMany(WorkTask::class, ['work_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkUser()
    {
        return $this->hasOne(User::class, ['id' => 'work_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['model_id' => 'id'])
            ->andWhere(['model' => Task::MODEL_WORK]);
    }

    public function createTask()
    {
        $task = new Task([
            'machine_id' => $this->machine_id,
            'user_id' => $this->work_user_id,
            'model' => Task::MODEL_WORK,
            'model_id' => $this->id,
            'status' => Task::STATUS_NOT_COMPLETED,
        ]);

        if ($this->planned_ended_at) {
            $task->planned_completed_at = $this->planned_ended_at;
        } elseif ($this->planned_product_count) {
            $task->planned_product_count = $this->planned_product_count;
        }

        $task->save();
    }

    public function updateTask()
    {
        $model = $this->task;

        if ($model) {
            // Обновляем статус
            switch ($this->status) {
                case static::STATUS_CREATED:
                    $model->status = Task::STATUS_NOT_COMPLETED;
                    break;
                case static::STATUS_IN_PROCESS:
                    $model->status = Task::STATUS_PROCESS;
                    break;
                case static::STATUS_TEST:
                    $model->status = Task::STATUS_TEST;
                    break;
                case static::STATUS_COMPLETED:
                    $model->status = Task::STATUS_COMPLETED;
                    break;
                case static::STATUS_REJECTED:
                    $model->status = Task::STATUS_REJECTED;
                    break;
            }

            // Обновляем ответственного
            if ($this->work_user_id != $model->user_id) {
                $model->user_id = $this->work_user_id;
            }

            // Обновляем планируемую дату выполнения
            if ($this->planned_ended_at != $model->planned_completed_at) {
                $model->planned_completed_at = $this->planned_ended_at;
            }

            $model->save();
        }
    }

    public function getName()
    {
        return $this->getTypeName();
    }

    public function isCompleted()
    {
        $isCompleted = true;

        if ($this->type == static::TYPE_PLANNED) {
            $isCompleted = !$this->getWorkTasks()
                ->andWhere(['status' => WorkTask::STATUS_NOT_COMPLETED])
                ->exists();
        }

        return $isCompleted;
    }

    public function getPlannedAtStr()
    {
        $str = '';

        if (!empty($this->planned_ended_at)) { // Если есть запланированная дата ТО
            return Yii::$app->formatter->asDatetime($this->planned_ended_at);
        } elseif (!empty($this->planned_product_count)) {  // Если ТО планируется по количеству произв-ых изделий
            $str = $this->planned_product_count . ' шт.';
            $producedCountDiff = $this->planned_product_count - $this->machine->produced_count;
            if ($producedCountDiff > 0) {
                $str .= ' (до выполнения ТО осталось изготовить – ';
                $str .= $producedCountDiff > 0 ? $producedCountDiff : 0;
                $str .= ' шт)';
            }
        }

        return $str;
    }

    public function getStatusLabel()
    {
        $labelClass = 'default';
        switch ($this->status) {
            case static::STATUS_CREATED:
                $labelClass = 'warning';
                break;
            case static::STATUS_IN_PROCESS:
                $labelClass = 'primary';
                break;
            case static::STATUS_TEST:
                $labelClass = 'warning';
                break;
            case static::STATUS_COMPLETED:
                $labelClass = 'success';
                break;
            case static::STATUS_REJECTED:
                $labelClass = 'danger';
                break;
        }

        return Html::tag('span', $this->getStatusName(),
            ['class' => 'label label-' . $labelClass]);
    }
}
