<?php

namespace app\models\data;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "examination".
 *
 * @property int $id
 * @property int $machine_id Оборудование
 * @property int $date Дата освидетельствования
 * @property string $document_number Номер документа
 * @property int $status Статус
 * @property int $notify_in_days Предупредить за n дней
 * @property int $next_date Дата следующего освидетельствования
 * @property string $notes Заметки
 * @property int $created_at
 * @property int $updated_at
 * @property int $created_by
 * @property int $updated_by
 *
 * @property Machine $machine
 * @property Task $task
 */
class Examination extends \yii\db\ActiveRecord
{
    const STATUS_NOT_COMPLETED = 0;
    const STATUS_COMPLETED = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'examination';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
            ],
            [
                'class' => BlameableBehavior::class
            ]
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (key_exists('status',
                $changedAttributes) && $this->status == static::STATUS_COMPLETED && !empty($this->next_date)
        ) {
            $nextExamination = new Examination([
                'machine_id' => $this->machine_id,
                'date' => $this->next_date,
                'status' => Examination::STATUS_NOT_COMPLETED,
                'document_number' => 'не указан',
            ]);

            if (!$nextExamination->save()) {
                var_dump($nextExamination->errors);
            }
        }

        if ($insert) {
            $this->createTask();
        }

        $this->updateTask();

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['machine_id', 'date', 'document_number', 'date', 'dateStr'], 'required'],
            [
                [
                    'machine_id',
                    'date',
                    'status',
                    'notify_in_days',
                    'created_at',
                    'updated_at',
                    'created_by',
                    'updated_by',
                    'next_date'
                ],
                'integer'
            ],
            [['document_number'], 'string', 'max' => 255],
            [['notes'], 'string', 'max' => 1000],
            [
                ['machine_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Machine::class,
                'targetAttribute' => ['machine_id' => 'id']
            ],
            [['dateStr', 'nextDateStr'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'machine_id' => 'Оборудование',
            'date' => 'Дата освидетельствования',
            'dateStr' => 'Дата освидетельствования',
            'next_date' => 'Дата следующего освидетельствования',
            'nextDateStr' => 'Дата следующего освидетельствования',
            'document_number' => 'Номер документа',
            'status' => 'Статус',
            'statusName' => 'Статус',
            'notify_in_days' => 'Предупредить за (дн)',
            'notes' => 'Заметки',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMachine()
    {
        return $this->hasOne(Machine::class, ['id' => 'machine_id']);
    }

    public function getDateStr()
    {
        return $this->date ? date('d.m.Y', $this->date) : null;
    }

    public function setDateStr($val)
    {
        $this->date = !empty($val) ? strtotime($val) : null;
    }

    public function getNextDateStr()
    {
        return $this->next_date ? date('d.m.Y', $this->next_date) : null;
    }

    public function setNextDateStr($val)
    {
        $this->next_date = !empty($val) ? strtotime($val) : null;
    }

    public static function getStatusList()
    {
        return [
            static::STATUS_NOT_COMPLETED => 'Не проведено',
            static::STATUS_COMPLETED => 'Проведено'
        ];
    }

    public function getStatusName()
    {
        $arr = static::getStatusList();
        return $arr[$this->status];
    }

    public function getName()
    {
        return 'Освидетельствование от ' . Yii::$app->formatter->asDate($this->date);
    }

    public function getUrl()
    {
        return Url::to(['/examination/view', 'id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::class, ['model_id' => 'id'])
            ->andWhere(['model' => Task::MODEL_EXAMINATION]);
    }

    public function createTask()
    {
        $task = new Task([
            'machine_id' => $this->machine_id,
            'user_id' => $this->machine->responsible_user_id,
            'model' => Task::MODEL_EXAMINATION,
            'model_id' => $this->id,
            'status' => Task::STATUS_NOT_COMPLETED,
            'planned_completed_at' => $this->date
        ]);

        $task->save();
    }

    public function updateTask()
    {
        $model = $this->task;

        if ($model) {
            // Обновляем статус
            switch ($this->status) {
                case static::STATUS_NOT_COMPLETED:
                    $model->status = Task::STATUS_NOT_COMPLETED;
                    break;
                case static::STATUS_COMPLETED:
                    $model->status = Task::STATUS_COMPLETED;
                    break;
            }

            // Обновляем планируемую дату выполнения
            if ($this->date != $model->planned_completed_at) {
                $model->planned_completed_at = $this->date;
            }

            $model->save();
        }
    }
}
