<?php

namespace app\models\notification;

use webzop\notifications\channels\EmailChannel;
use Yii;
use app\models\data\User;
use webzop\notifications\Notification;
use app\models\data\Task;
use yii\helpers\Url;

class WorkNotification extends Notification
{
    const DAILY_FOR_ENGINEERS = 'daily_for_engineers';
    const DAILY_FOR_CHIEF_ENGINEER = 'daily_for_chief_engineer';

    /**
     * @var User
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function getTitle()
    {
        switch ($this->key) {
            case self::DAILY_FOR_ENGINEERS:
                return 'Текущие задачи для инженера';
                break;
            case self::DAILY_FOR_CHIEF_ENGINEER:
                return 'Текущие задачи для главного инженера';
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        switch ($this->key) {
            case self::DAILY_FOR_ENGINEERS:
                return 'Список текущих задач для инженера';
                break;
            case self::DAILY_FOR_CHIEF_ENGINEER:
                return 'Список текущих задач для главного инженера';
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function getRoute()
    {
        switch ($this->key) {
            case self::DAILY_FOR_ENGINEERS:
                return Url::to([
                    '/task/index',
                    'TaskSearch' => [
                        'user_id' => $this->user->id,
                        'status' => [Task::STATUS_NOT_COMPLETED, Task::STATUS_PROCESS]
                    ]
                ], true);
                break;
            case self::DAILY_FOR_CHIEF_ENGINEER:
                return Url::to([
                    '/task/index',
                    'TaskSearch' => [
                        'status' => [Task::STATUS_NOT_COMPLETED, Task::STATUS_PROCESS, Task::STATUS_TEST]
                    ]
                ], true);
                break;
        }
    }

    /**
     * Override send to email channel
     *
     * @param $channel EmailChannel
     * @return void
     */
    public function toEmail($channel)
    {
        switch ($this->key) {
            case self::DAILY_FOR_ENGINEERS:
                $subject = $this->getTitle();
                $template = self::DAILY_FOR_ENGINEERS;
                break;
            case self::DAILY_FOR_CHIEF_ENGINEER:
                $subject = $this->getTitle();
                $template = self::DAILY_FOR_CHIEF_ENGINEER;
                break;
        }

        $message = $channel->mailer->compose($template, [
            'user' => $this->user,
            'notification' => $this,
        ]);
        Yii::configure($message, $channel->message);

        $message->setTo($this->user->email);
        $message->setSubject($subject);

//        if (YII_ENV_PROD) {
        $message->send($channel->mailer);
//        }
    }

    public function shouldSend($channel)
    {
        if ($channel->id == 'screen') {
            if (in_array($this->key, [self::DAILY_FOR_ENGINEERS])) {
                return false;
            }
        }

        return true;
    }
}