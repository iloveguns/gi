<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\data\Task;

/**
 * TaskSearch represents the model behind the search form of `app\models\data\Task`.
 */
class TaskSearch extends Task
{
    // Поиск недавних задач
    var $recent = false;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'model_id', 'machine_id', 'user_id', 'created_at', 'updated_at'], 'integer'],
            [['model', 'notes', 'status', 'recent'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Task::find();

        if ($this->recent) {
            $query->joinWith('machine');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => ['created_at' => SORT_DESC]
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'machine_id' => $this->machine_id,
            'task.status' => $this->status,
            'user_id' => $this->user_id,
            'model_id' => $this->model_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'model', $this->model])
            ->andFilterWhere(['like', 'notes', $this->notes]);

        if ($this->recent) {
            // Задачи, выполнение которых необходимо за последние 14 дней
            // или задачи, до выполнения которых осталось изготовить < 100 деталей
            $query->andFilterWhere([
                'OR',
                [
                    'AND',
                    'task.planned_completed_at IS NOT NULL',
                    ['<', 'task.planned_completed_at', time() + 14 * 24 * 3600]
                ],
                [
                    'AND',
                    'task.planned_product_count IS NOT NULL',
                    ['<', '(task.planned_product_count - machine.produced_count)', 100]
                ]
            ]);
        }

        return $dataProvider;
    }
}
