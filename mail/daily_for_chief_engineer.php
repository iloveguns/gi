<?php
/**
 * @var $notification \app\models\notification\WorkNotification
 */
?>

<tr>
    <td align="center" valign="top" width="100%" style="background-color: #f7f7f7;" class="content-padding">
        <center>
            <table cellspacing="0" cellpadding="0" width="600" class="w320">
                <tr>
                    <td class="header-lg">
                        Здравствуйте, <?= $user->profile->name ?>!
                    </td>
                </tr>
                <tr>
                    <td class="free-text">
                        Нажмите на кнопку, чтобы перейти к списку текущих задач.
                    </td>
                </tr>
                <tr>
                    <td class="button">
                        <div><a class="button-mobile" href="<?= $notification->getRoute() ?>"
                                style="background-color:#ff6f6f;border-radius:5px;color:#ffffff;display:inline-block;font-family:'Cabin', Helvetica, Arial, sans-serif;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Открыть список задач</a></div>
                    </td>
                </tr>
            </table>
        </center>
    </td>
</tr>
